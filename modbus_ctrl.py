"""
  ------------------------------ JT400 Modbus Map ------------------------------------------------

    | Address |       Description                 |               Size              |   Initial value   |
        5	    ASCII serial number	                    4 bytes, unsigned integer
        11	    ASCII Fw Revision	                    4 bytes, unsigned integer	    J4xxx.xxx, J6xxx.xxx rev
        15	    JT400 unit address (Modbus, BSAP)	    Byte, signed integer	1
        16	    JT400 DP Range Code	                    Byte, signed integer	1
        17	    JT400 SP Range Code	                    Byte, signed integer	14
        35	    ASCII user description	                16 Bytes, ASCII	space
        36	    ASCII DP zero Calibration date	        10 Bytes, ASCII	space
        37	    ASCII DP Span Calibration date	        10 Bytes, ASCII	space
        38	    ASCII SP zero Calibration date	        10 Bytes, ASCII	space
        39	    ASCII SP Span Calibration date	        10 Bytes, ASCII	space
        40	    ASCII RTD zero Calibration date	        10 Bytes, ASCII	space
        41	    ASCII RTD Span Calibration date	        10 Bytes, ASCII	space
        59	    DP unit code	                        Byte, signed integer	        InH2O
        60	    SP unit code	                        Byte, signed integer	        PSI
        61	    RTD Unit Code	                        Byte, signed integer	        C
        115	    JT400 Hardware Rev	                    Byte, signed integer
        118	    JT400 Device Status 0	                byte, signed integer	        0
        119	    JT400 Device Status 1	                byte, signed integer	        0
        120	    JT400 Device Status 2	                byte, signed integer	        0
        121	    JT400 Device Status 3	                byte, signed integer	        0
        122	    JT400 Device Status 4	                byte, signed integer	        0
        123	    JT400 Device Status 5	                byte, signed integer	        0
        126	    RS584/RS232 RTS delay interval	        2 bytes, signed integer	        3
        127	    LCD enable/disable control bits	        2 bytes, signed integer	        127
        128	    LCD holding time interval	            2 bytes, signed integer	        3
        137	    LCD enable/disable control bits	        2 bytes, signed integer	        0
        144	    JT400 D_IO status 	                    2 bytes, signed integer	        0
        145	    JT400 D_IO control	                    2 bytes, signed integer	        0
        7391	DP Span Calibration Left Value	        4 bytes, floating
        7392	SP Span Calibration Left Value	        4 bytes, floating
        7393	RTD Span Calibration Left Value	        4 bytes, floating
        7394	DP Zero Calibration Left Value	        4 bytes, floating
        7395	SP Zero Calibration Left Value	        4 bytes, floating
        7396	RTD Zero Calibration Left Value	        4 bytes, floating
        7397	Orifice Plate diameter	                4 bytes, floating
        7398	Pipe diameter	                        4 bytes, floating
        7399	Sqrt(DP*SP) value	                    4 bytes, floating
        7400	Sensor 1 DP readings	                4 bytes, floating	            depends on the sensor
        7401	Sensor 1 SP readings	                4 bytes, floating	            depends on the sensor
        7402	JT400 Temperature (RTD) reading	        4 bytes, floating	            depends on the sensor
        7403	JT400 status 0	                        4 bytes, unsigned interger	    0
        7404	JT400 status 2	                        4 bytes, unsigned interger	    0
        7405	JT400 status 4	                        4 bytes, unsigned interger	    0

"""
import logging
import pprint
import random
import threading
import traceback
import modbus_tk
import serial
import time
from modbus_tk import modbus_rtu
import modbus_tk.defines as cst
import utils
from base import Base


modbus_logger = logging.getLogger("airtu.modbus")
modbus_logger.setLevel(logging.DEBUG)
modbus_logger.addHandler(utils.console_handler)


class ModbusCtrl(Base):
    ctrl = None
    lock = threading.RLock()

    def __init__(self, port='/dev/ttyS0',
                 baudrate=9600, byte_size=8, parity='N', stop_bits=1, xonxoff=0, timeout=.5, verbose=True):
        Base.__init__(self)

        try:
            self.ctrl = modbus_rtu.RtuMaster(serial.Serial(port=port, baudrate=baudrate, bytesize=byte_size,
                                                           parity=parity, stopbits=stop_bits,
                                                           xonxoff=xonxoff))
            self.ctrl.set_timeout(timeout)
            self.ctrl.set_verbose(verbose)
        except serial.SerialException as e:
            traceback.print_exc()
            modbus_logger.error(e)
            self.ctrl = None

    def read_sensor_values(self, slave_addr):
        """
        Read DP, SP, RTD from JT400 device
        :return:
        """
        # See register map above.
        val = self.read_holding_register(slave_addr, 7400, 3)
        if val is None:
            return None
        else:
            # TODO: Convert to human-readable value
            result = dict()
            result['DP'] = val[0]
            result['SP'] = val[1]
            result['RTD'] = val[2]
            return result

    def read_holding_register(self, slave_addr, reg_addr, length):
        """
        Read holding registers with given length
        :param reg_addr: starting register address
        :param length: Reading length
        :return: Tuple of register values
        """
        if self.debug:
            val = []
            for i in range(length):
                val.append(random.randint(1, 1000))
            return tuple(val)

        if self.ctrl is None:
            return None

        # TODO: Add more comprehensive exception code since modbus_tk is not so stable(I feel so...)
        with self.lock:
            try:
                val = self.ctrl.execute(slave_addr, cst.READ_HOLDING_REGISTERS, reg_addr, length)
                return val
            except modbus_tk.modbus.ModbusInvalidResponseError as e:
                modbus_logger.error(e)
                print e
                return None
            except OSError as e:
                modbus_logger.error(e)
                print e
                return None
            except:
                traceback.print_exc()
                modbus_logger.error(traceback.format_exc())
                return None

    def write_holding_register(self, slave_addr, reg_addr, value):
        """
        Write a value to given register.
        :param reg_addr:
        :param value:
        :return:
        """
        if self.ctrl is None:
            return False

        # TODO: Add more comprehensive exception code since modbus_tk is not so stable(I feel so...)
        with self.lock:
            try:
                # TODO: get return value of this function and see it is executed correctly...
                #  Not sure why the response does not match...
                #  Though I set register 101 as 1, its response was always (101, 0)
                self.ctrl.execute(slave_addr, cst.WRITE_SINGLE_REGISTER, reg_addr, output_value=value)
                # print 'Return value of writing command: ', return_val
                return True
            except modbus_tk.modbus.ModbusInvalidResponseError as e:
                modbus_logger.error(e)
                print e
                return False
            except OSError as e:
                modbus_logger.error(e)
                print e
                return False
            except:
                traceback.print_exc()
                modbus_logger.error(traceback.format_exc())
                return False


if __name__ == '__main__':
    # a = ModbusCtrl(port='/dev/serial/by-id/usb-FTDI_USB-RS485_Cable_FT08L7DJ-if00-port0')
    a = ModbusCtrl(port='/dev/serial/by-id/usb-FTDI_USB-RS485_Cable_FTZ29SUI-if00-port0')
    while True:
        s_time = time.time()
        pprint.pprint(a.read_sensor_values(3))
        if time.time() - s_time < 1:
            time.sleep(1 + s_time - time.time())
        else:
            print 'Warning, it took more than a second to read values!'
