"""
Calculates AGA3 Flow based using gas compositions and AGA8 calculations.
Pressure units are in KPA.
Temperature units are in Celsius.
Length units are in mm.

"""
import math
import pprint
import random

import time

from aga8 import AGA8
import unit_converter

# ---------------------------- Constant Values for AGA3 calculation ---------------------------------------
A = 4.343524261523267
B = 3.764387693320165

A0 = 0.5961
A1 = 0.0291
A2 = -0.229
A3 = 0.003
A4 = 2.8
A5 = 0.000511
A6 = 0.021

S1 = 0.0049
S2 = 0.0433
S3 = 0.0712
S4 = -0.1145
S5 = -0.2300
S6 = -0.0116
S7 = -0.5200
S8 = -0.1400

k = 1.3

AirConstant = 28.9625

Rgas = 0.0831451
Nc = 0.036
Nic = 0.1
N3 = 1000.0
N4 = 25.4
N5 = 273.15

Xc = 1.142129337256165

meu = 0.010268

materials = ['stainlesssteel', 'monel', 'carbonsteel']
ThermalExpansionValues = [0.00001665, 0.0000143, 0.00001116]


# ---------------- Constant/Input values to be changed -----------------------
b_aga8 = False

orificeSizeMM = 12.7
pipeSizeMM = 50.8
tapIsUpstream = True

FlowZCalc = 1.3
BaseZCalc = 1.3
SpecificGravity = 0.61

baseStaticPressureKPA = 101.33
baseTemperatureInKelvin = 288.15
orificeReferenceTemperatureInKelvin = 288.15
pipeReferenceTemperatureInKelvin = 288.15

orificeMaterial = 'StainlessSteel'      # Monel or CarbonSteel
pipeMaterial = 'StainlessSteel'         # Monel or CarbonSteel

gasCompose = {
    'methane': 90.6724,
    'nitrogen': 3.1284,
    'carbonDioxide': 0.4676,
    'ethane': 4.5279,
    'propane': 0.828,
    'water': 0,
    'hydrogenSulfide': 0,
    'hydrogen': 0,
    'carbonMonoxide': 0,
    'oxygen': 0,
    'iButane': 0.1037,
    'nButane': 0.1563,
    'iPentane': 0.0321,
    'nPentane': 0.0443,
    'nHexane': 0.0393,
    'nHeptane': 0.0,
    'nOctane': 0,
    'nNonane': 0,
    'nDecane': 0,
    'helium': 0.01,
    'argon': 0
}
# ---------------------------------------------------------------------------------------------

QCP_time = 5        # QCP calculation time in minutes
QTR_time = 60       # QTR calculation time in minutes

orificeexpansionCoefficient = ThermalExpansionValues[materials.index(orificeMaterial.lower())]
pipeexpansionCoefficient = ThermalExpansionValues[materials.index(pipeMaterial.lower())]


def CalculateFlow(flowTemperatureInKelvin,
                  pipeReferenceTemperatureInKelvin,
                  orificeReferenceTemperatureInKelvin,
                  baseTemperatureInKelvin,
                  staticPressureKPA,
                  baseStaticPressureKPA,
                  differentialPressureKPA,
                  orificeSizeMM,
                  pipeSizeMM,
                  orificeexpansionCoefficient,
                  pipeexpansionCoefficient,
                  tapIsUpstream,
                  FlowZCalc,
                  BaseZCalc,
                  SpecificGravity):
    
    d = orificeSizeMM * (1.0 + orificeexpansionCoefficient * (flowTemperatureInKelvin - orificeReferenceTemperatureInKelvin))

    D = pipeSizeMM * (1.0 + pipeexpansionCoefficient * (flowTemperatureInKelvin - pipeReferenceTemperatureInKelvin))
    
    Beta = d / D
    
    Ev = 1.0 / math.sqrt(1.0 - math.pow(Beta, 4))
    
    L1 = N4 / D
    
    L2 = N4 / D
    
    M2 = (2.0 * L2) / (1.0 - Beta)
    
    Tu = (S2 + S3 * math.exp(-8.5 * L1) + S4 * math.exp(-6.0 * L1)) * (math.pow(Beta, 4) / (1 - math.pow(Beta, 4)))
    
    Td = S6 * (M2 + S7 * math.pow(M2, 1.3)) * math.pow(Beta, 1.1)
    
    diffPressMillibar = unit_converter.KPAToMillibar(differentialPressureKPA)

    staticPressBar = unit_converter.KPAToBar(staticPressureKPA)

    if not tapIsUpstream:
        staticPressBar = staticPressBar + unit_converter.MillibarToBar(diffPressMillibar)

    if D > (A4 * N4):
        Ts = 0.0
    else:
        Ts = A3 * (1.0 - Beta) * (A4 - (D / N4))

    # XQ9211.pdf Page: 34
    # Procedure 4.3.2.5 Calculation of Flange - Tapped Orifice Plate
    # Coefficient of Discharge Constants

    Cd0 = A0 + A1 * math.pow(Beta, 2) + A2 * math.pow(Beta, 8) + Tu + Td + Ts

    Cd1 = A5 * math.pow(Beta, 0.7) * math.pow(250, 0.7)

    Cd2 = A6 * math.pow(Beta, 4) * math.pow(250, 0.35)

    Cd3 = S1 * math.pow(Beta, 4) * math.pow(Beta, 0.8) * math.pow(4.75, 0.8) * math.pow(250, 0.35)

    Cd4 = (S5 * Tu + S8 * Td) * math.pow(Beta, 0.8) * math.pow(4.75, 0.8)

    X1 = diffPressMillibar / (N3 * staticPressBar)

    Yp = (0.41 + 0.35 * math.pow(Beta, 4)) / k

    Y = 1.0 - (Yp * X1)

    Ftmp = Ev * Y * d * d

    density = ((SpecificGravity * AirConstant / Rgas) * staticPressBar) / (FlowZCalc * flowTemperatureInKelvin)

    # XQ9211.pdf Page: 37
    # Procedure 4.3.2.8 Calculation of Iteration Flow Factor

    FIc = (4000.0 * Nic * D * meu) / Ftmp

    Fip = math.sqrt(2.0 * density * diffPressMillibar)

    if FIc < 1000.0 * Fip and Fip > 0:
        Fi = FIc / Fip
    else:
        Fi = 1000.0

    Cdft = Cd0

    deltaCd = 2.0

    for i in range(0, 50):
        if math.fabs(deltaCd) <= 0.0000005:
            break
        
        X2 = Fi / Cdft

        if X2 < Xc:
            Fc = Cd0 + (Cd1 * math.pow(X2, 0.35) + Cd2 + Cd3 * math.pow(X2, 0.8)) * math.pow(X2, 0.35) + Cd4 * math.pow(X2, 0.8)
            Dc = (0.7 * math.pow(Cd1, 0.35) + 0.35 * Cd2 + 1.15 * math.pow(X2, 0.8)) * math.pow(X2, 0.35) + 0.8 * Cd4 * math.pow(X1, 0.8)
        else:
            Fc = Cd0 + Cd1 * math.pow(X2, 0.7) + (Cd2 + Cd3 * math.pow(X2, 0.8)) * (A - (B / X2)) + Cd4 * math.pow(X1, 0.98)
            Dc = 0.7 * Cd1 * math.pow(X2, 0.7) + (Cd2 + Cd3 * math.pow(X1, 0.8)) * B / X2 + 0.8 * Cd3 * (A - B / X2) * math.pow(X2, 0.8) + 0.8 * Cd4 * math.pow(X2, 0.8)

        deltaCd = (Cdft - Fc) / (1.0 + (Dc / Cdft))

        Cdft -= deltaCd

    densityBase = ((SpecificGravity * AirConstant / Rgas) * unit_converter.KPAToBar(baseStaticPressureKPA)) / (BaseZCalc * baseTemperatureInKelvin)

    mFLow = 24.0 * 3.14159 / 4.0 * Nc * (Ftmp / Y) * Cdft * Y * math.sqrt(2.0 * density * diffPressMillibar)

    formattedFlow = (mFLow / densityBase) / 1000.0

    results = dict()

    results['Flow'] = round(formattedFlow, 10)
    results['Compressibility_FPV'] = round(math.sqrt(BaseZCalc / FlowZCalc), 10)
    results['SpecificGravity'] = round(SpecificGravity, 10)
    results['BaseCompressibility_Zb'] = round(BaseZCalc, 10)
    results['FlowingCompressibility_Zf'] = round(FlowZCalc, 10)

    return results


def calculate_flow(staticPressureKPA, differentialPressureKPA, flowTemperatureInKelvin):
    global FlowZCalc
    global SpecificGravity
    global BaseZCalc
    # Main Procedure
    if b_aga8:
        # If AGA8 is enabled, we have to obtain 3 values from AGA8 module.
        aga8 = AGA8()

        _staticPressBar = unit_converter.KPAToBar(staticPressureKPA)
        if not tapIsUpstream:
            diffPressBar = unit_converter.KPAToBar(differentialPressureKPA)
            _staticPressBar += diffPressBar

        flow_result = aga8.CalculateZ(gasCompositions=gasCompose, temperatureInKelvin=flowTemperatureInKelvin,
                                      pressurePSI=unit_converter.KPAtoPSI(unit_converter.BarToKPA(_staticPressBar)))

        FlowZCalc = flow_result['FlowCompressiblity']
        SpecificGravity = flow_result['SpecificGravity']

        base_result = aga8.CalculateZ(gasCompositions=gasCompose, temperatureInKelvin=baseTemperatureInKelvin,
                                      pressurePSI=unit_converter.KPAtoPSI(baseStaticPressureKPA))
        BaseZCalc = base_result['FlowCompressiblity']

    # Calculate Flow value.
    result = CalculateFlow(flowTemperatureInKelvin=flowTemperatureInKelvin,
                           pipeReferenceTemperatureInKelvin=pipeReferenceTemperatureInKelvin,
                           orificeReferenceTemperatureInKelvin=orificeReferenceTemperatureInKelvin,
                           baseTemperatureInKelvin=baseTemperatureInKelvin,
                           staticPressureKPA=staticPressureKPA,
                           baseStaticPressureKPA=baseStaticPressureKPA,
                           differentialPressureKPA=differentialPressureKPA,
                           orificeSizeMM=orificeSizeMM,
                           pipeSizeMM=pipeSizeMM,
                           orificeexpansionCoefficient=orificeexpansionCoefficient,
                           pipeexpansionCoefficient=pipeexpansionCoefficient,
                           tapIsUpstream=tapIsUpstream,
                           FlowZCalc=FlowZCalc,
                           BaseZCalc=BaseZCalc,
                           SpecificGravity=SpecificGravity)
    # Calculated flow is in unit of E3M3/Day, so let's convert it to E3M3/sec
    result['Flow'] = result['Flow'] / 24.0 / 60.0 / 60.0
    return result


if __name__ == '__main__':

    # Initial values
    seconds = 0
    QCP_val = 0
    INTEGRAL_val = 0
    AVERAGE_sp = 0
    AVERAGE_dp = 0
    AVERAGE_temp = 0

    while True:
        s_time = time.time()
        # Generate random input values
        staticPressureKPA = 100.0 + random.randint(1, 10) - 5.0                 # 100 +/- 5
        differentialPressureKPA = 13 + random.randint(1, 10) / 10.0 - .5        # 13 +/- .5
        flowTemperatureInKelvin = 303.15 + random.randint(1, 10) - 5            # 303.15 +/ 5

        result = calculate_flow(staticPressureKPA, differentialPressureKPA, flowTemperatureInKelvin)

        print '-' * 40
        print 'Seconds: ', seconds
        staticPressurePSI = unit_converter.KPAtoPSI(staticPressureKPA)
        print 'Static Pressure: %.7f (PSI)' % staticPressurePSI
        differentialPressureINH2O = unit_converter.KPAtoINH2O(differentialPressureKPA)
        print 'Differential Pressure: %.7f (inH2O)' % differentialPressureINH2O
        flowTemperatureInF = unit_converter.CELCIUStoFARHENHEIT(flowTemperatureInKelvin - 273.15)
        print 'Temperature: %.7f (F)' % flowTemperatureInF
        print 'Current Flow: %.7f (E3M3/Sec) = %.7f (MCF/Sec)' % (
            result['Flow'], unit_converter.E3M3toMFC(result['Flow']))

        if seconds % (QCP_time * 60) == 0:
            # Initialize at the beginning of QCP
            QCP_val = 0
        else:
            QCP_val += result['Flow']
        print 'Quantity accumulated over the QCP: %.7f' % QCP_val

        if seconds % (QTR_time * 60) == 0:
            # Initialize at the beginning of QTR
            INTEGRAL_val = math.sqrt(staticPressurePSI * differentialPressureINH2O)
            AVERAGE_sp = staticPressurePSI
            AVERAGE_dp = differentialPressureINH2O
            AVERAGE_temp = flowTemperatureInF
        else:
            INTEGRAL_val += math.sqrt(staticPressurePSI * differentialPressureINH2O)
            num = seconds % (QTR_time * 60)
            AVERAGE_sp = (AVERAGE_sp * num + staticPressurePSI) / float(num + 1)
            AVERAGE_dp = (AVERAGE_dp * num + differentialPressureINH2O) / float(num + 1)
            AVERAGE_temp = (AVERAGE_temp * num + flowTemperatureInF) / float(num + 1)

        print 'Integrated Value: %.7f' % INTEGRAL_val
        print 'Average Static Pressure: %.7f (PSI)' % AVERAGE_sp
        print 'Average Differential Pressure: %.7f (INH20)' % AVERAGE_dp
        print 'Average Flow Temperature: %.7f (F)' % AVERAGE_temp

        seconds += 1

        if time.time() - s_time < 1:
            time.sleep(1 + s_time - time.time())
