# -*- coding: utf-8 -*-
"""
Proxy Server: 54.173.39.231
Upboard: kelvin2@localhost -p8814

"""

import glob
import os
import pprint
import sys

from kivy.app import App
from kivy.lang import Builder

from kivy.properties import StringProperty, ListProperty, ObjectProperty
from kivy.uix.button import Button
from kivy.uix.dropdown import DropDown
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy.uix.screenmanager import SlideTransition
from kivy.config import Config

import unit_converter
from aga8 import AGA8
import aga3


class CautionPopup(Popup):
    pass


class ConfirmPopup(Popup):
    pass


class CompositionPopup(Popup):
    pass


class MainApp(App):
    current_title = StringProperty()        # Store title of current screen
    screen_names = ListProperty([])
    screens = {}                            # Dict of all screens
    hierarchy = ListProperty([])

    # Popup instances
    caution_popup = ObjectProperty(None)
    confirm_popup = ObjectProperty(None)
    composition_popup = ObjectProperty(None)

    dd_orifice_diameter = DropDown()
    dd_pipe_diameter = DropDown()
    dd_orifice_material = DropDown()
    dd_pipe_material = DropDown()

    def build(self):
        """
        base function of kivy app
        :return:
        """

        self.load_screen()

        self.caution_popup = CautionPopup()
        self.confirm_popup = ConfirmPopup()
        self.composition_popup = CompositionPopup()

        self.add_dropdown()

        self.go_screen('menu', 'right')

    def confirm_yes(self):
        """
        callback function of "Yes" button in confirm popup
        :return:
        """
        self.confirm_popup.dismiss()

    def confirm_no(self):
        """
        callback function of "No" button in confirm popup
        :return:
        """
        self.confirm_popup.dismiss()

    def add_dropdown(self):
        # Add Orifice Diameter values
        for i in range(1, 33):
            txt = str(3.175 * i) + ' mm/' + str(.125 * i) + ' in'
            btn = Button(text=txt, size_hint_y=None, height=50)
            btn.bind(on_release=lambda b: self.dd_orifice_diameter.select(b.text))
            self.dd_orifice_diameter.add_widget(btn)
        dd_btn1 = self.screens['menu'].ids['btn_OrificeDiameter']
        dd_btn1.bind(on_release=self.dd_orifice_diameter.open)
        self.dd_orifice_diameter.bind(on_select=lambda instance, x: setattr(dd_btn1, 'text', x))

        # Add Pipe Diameter values
        for i in range(16, 33):
            txt = str(3.175 * i) + ' mm/' + str(.125 * i) + ' in'
            btn = Button(text=txt, size_hint_y=None, height=50)
            btn.bind(on_release=lambda b: self.dd_pipe_diameter.select(b.text))
            self.dd_pipe_diameter.add_widget(btn)
        dd_btn2 = self.screens['menu'].ids['btn_PipeDiameter']
        dd_btn2.bind(on_release=self.dd_pipe_diameter.open)
        self.dd_pipe_diameter.bind(on_select=lambda instance, x: setattr(dd_btn2, 'text', x))

        # Add Orifice Material values
        for txt in ['StainlessSteel', 'Monel', 'CarbonSteel']:
            btn = Button(text=txt, size_hint_y=None, height=40)
            btn.bind(on_release=lambda b: self.dd_orifice_material.select(b.text))
            self.dd_orifice_material.add_widget(btn)
        dd_btn3 = self.screens['settings'].ids['btn_OrificeMaterial']
        dd_btn3.bind(on_release=self.dd_orifice_material.open)
        self.dd_orifice_material.bind(on_select=lambda instance, x: setattr(dd_btn3, 'text', x))

        # Add Pipe Material values
        for txt in ['StainlessSteel', 'Monel', 'CarbonSteel']:
            btn = Button(text=txt, size_hint_y=None, height=40)
            btn.bind(on_release=lambda b: self.dd_pipe_material.select(b.text))
            self.dd_pipe_material.add_widget(btn)
        dd_btn4 = self.screens['settings'].ids['btn_PipeMaterial']
        dd_btn4.bind(on_release=self.dd_pipe_material.open)
        self.dd_pipe_material.bind(on_select=lambda instance, x: setattr(dd_btn4, 'text', x))

    def go_screen(self, dest_screen, direction):
        """
        Go to given screen
        :param dest_screen:     destination screen name
        :param direction:       "up", "down", "right", "left"
        :return:
        """
        sm = self.root.ids.sm

        sm.transition = SlideTransition()
        screen = self.screens[dest_screen]
        sm.switch_to(screen, direction=direction)
        self.current_title = screen.name

    def load_screen(self):
        """
        Load all screens from data/screens to Screen Manager
        :return:
        """
        available_screens = []

        full_path_screens = glob.glob("screens/*.kv")

        for file_path in full_path_screens:
            file_name = os.path.basename(file_path)
            available_screens.append(file_name.split(".")[0])

        self.screen_names = available_screens
        for i in range(len(full_path_screens)):
            screen = Builder.load_file(full_path_screens[i])
            self.screens[available_screens[i]] = screen
        return True

    def calculate(self):
        # ---------------- Constant/Input values to be changed -----------------------
        b_aga8 = self.screens['settings'].ids['chk_AGA8'].active

        staticPressureKPA = float(self.screens['menu'].ids['txt_StaticPressureKPA'].text)
        differentialPressureKPA = float(self.screens['menu'].ids['txt_DifferentialPressureKPA'].text)
        flowTemperatureInKelvin = float(self.screens['menu'].ids['txt_FlowingTemperatureC'].text) + 273.15

        orificeSizeMM = float(self.screens['menu'].ids['btn_OrificeDiameter'].text.split('mm')[0])
        pipeSizeMM = float(self.screens['menu'].ids['btn_PipeDiameter'].text.split('mm')[0])
        tapIsUpstream = self.screens['menu'].ids['chk_TapIsUpstream'].active

        FlowZCalc = float(self.screens['settings'].ids['txt_FlowingCompressibility'].text)
        BaseZCalc = float(self.screens['settings'].ids['txt_BaseCompressibility'].text)
        SpecificGravity = float(self.screens['settings'].ids['txt_SpecificGravity'].text)

        baseStaticPressureKPA = float(self.screens['settings'].ids['txt_BasePressureKPA'].text)
        baseTemperatureInKelvin = float(self.screens['settings'].ids['txt_BaseTemperatureC'].text) + 273.15
        orificeReferenceTemperatureInKelvin = float(
            self.screens['settings'].ids['txt_OrificeTemperatureC'].text) + 273.15
        pipeReferenceTemperatureInKelvin = float(self.screens['settings'].ids['txt_PipeTemperatureC'].text) + 273.15

        orificeMaterial = self.screens['settings'].ids['btn_OrificeMaterial'].text
        pipeMaterial = self.screens['settings'].ids['btn_PipeMaterial'].text

        gasCompose = {
            'methane': float(self.composition_popup.ids['txt_methane'].text),
            'nitrogen': float(self.composition_popup.ids['txt_nitrogen'].text),
            'carbonDioxide': float(self.composition_popup.ids['txt_carbonDioxide'].text),
            'ethane': float(self.composition_popup.ids['txt_ethane'].text),
            'propane': float(self.composition_popup.ids['txt_propane'].text),
            'water': float(self.composition_popup.ids['txt_water'].text),
            'hydrogenSulfide': float(self.composition_popup.ids['txt_hydrogenSulfide'].text),
            'hydrogen': float(self.composition_popup.ids['txt_hydrogen'].text),
            'carbonMonoxide': float(self.composition_popup.ids['txt_carbonMonoxide'].text),
            'oxygen': float(self.composition_popup.ids['txt_oxygen'].text),
            'iButane': float(self.composition_popup.ids['txt_iButane'].text),
            'nButane': float(self.composition_popup.ids['txt_nButane'].text),
            'iPentane': float(self.composition_popup.ids['txt_iPentane'].text),
            'nPentane': float(self.composition_popup.ids['txt_nPentane'].text),
            'nHexane': float(self.composition_popup.ids['txt_nHexane'].text),
            'nHeptane': float(self.composition_popup.ids['txt_nHeptane'].text),
            'nOctane': float(self.composition_popup.ids['txt_nOctane'].text),
            'nNonane': float(self.composition_popup.ids['txt_nNonane'].text),
            'nDecane': float(self.composition_popup.ids['txt_nDecane'].text),
            'helium': float(self.composition_popup.ids['txt_helium'].text),
            'argon': float(self.composition_popup.ids['txt_argon'].text)
        }

        # -----------------------------------------------------------------------------------

        orificeexpansionCoefficient = aga3.ThermalExpansionValues[aga3.materials.index(orificeMaterial.lower())]
        pipeexpansionCoefficient = aga3.ThermalExpansionValues[aga3.materials.index(pipeMaterial.lower())]

        # Main Procedure
        if b_aga8:

            # If AGA8 is enabled, we have to obtain 3 values from AGA8 module.

            aga8 = AGA8()

            _staticPressBar = unit_converter.KPAToBar(staticPressureKPA)
            if not tapIsUpstream:
                diffPressBar = unit_converter.KPAToBar(differentialPressureKPA)
                _staticPressBar += diffPressBar

            flow_result = aga8.CalculateZ(gasCompositions=gasCompose, temperatureInKelvin=flowTemperatureInKelvin,
                                          pressurePSI=unit_converter.KPAtoPSI(unit_converter.BarToKPA(_staticPressBar)))

            FlowZCalc = flow_result['FlowCompressiblity']
            SpecificGravity = flow_result['SpecificGravity']

            base_result = aga8.CalculateZ(gasCompositions=gasCompose, temperatureInKelvin=baseTemperatureInKelvin,
                                          pressurePSI=unit_converter.KPAtoPSI(baseStaticPressureKPA))
            BaseZCalc = base_result['FlowCompressiblity']

        # Calculate Flow value.
        result = aga3.CalculateFlow(flowTemperatureInKelvin=flowTemperatureInKelvin,
                                    pipeReferenceTemperatureInKelvin=pipeReferenceTemperatureInKelvin,
                                    orificeReferenceTemperatureInKelvin=orificeReferenceTemperatureInKelvin,
                                    baseTemperatureInKelvin=baseTemperatureInKelvin,
                                    staticPressureKPA=staticPressureKPA,
                                    baseStaticPressureKPA=baseStaticPressureKPA,
                                    differentialPressureKPA=differentialPressureKPA,
                                    orificeSizeMM=orificeSizeMM,
                                    pipeSizeMM=pipeSizeMM,
                                    orificeexpansionCoefficient=orificeexpansionCoefficient,
                                    pipeexpansionCoefficient=pipeexpansionCoefficient,
                                    tapIsUpstream=tapIsUpstream,
                                    FlowZCalc=FlowZCalc,
                                    BaseZCalc=BaseZCalc,
                                    SpecificGravity=SpecificGravity)

        pprint.pprint(result)

        Popup(title='Flow Result', title_size=20,
              content=Label(text='Flow: {} E3M3'.format(round(result['Flow'], 4)), font_size=30),
              size_hint=(None, None), size=(500, 200)).open()


if __name__ == '__main__':

    if Config.getint('graphics', 'width') != 1280:
        Config.set('graphics', 'width', '1280')
        Config.write()
        print 'Sorry, window width is not set correctly, please restart app now.'
        sys.exit()
    elif Config.getint('graphics', 'height') != 800:
        Config.set('graphics', 'height', '800')
        Config.write()
        print 'Sorry, window height is not set correctly, please restart app now.'
        sys.exit()

    app = MainApp()
    app.run()
