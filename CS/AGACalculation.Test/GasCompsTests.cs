﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Telerik.JustMock;


namespace AGACalculation.Test
{
    public class GasCompsTests
    {
        [Test]
        public void When_Gas_Comps_Constructor_Is_Called_With_InAccurate_Compositions()
        {
            AGAFlowCalculation.GasComps gascomps =
                new AGAFlowCalculation.GasComps(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

            bool valid = gascomps.IsGasCompositionValid();

            Assert.AreEqual(false, valid);
        }

        [Test]
        public void When_Gas_Comps_Constructor_Is_Called_With_Accurate_Compositions()
        {
            AGAFlowCalculation.GasComps gascomps =
                new AGAFlowCalculation.GasComps(.1, .1, .1, .1, .1, .1, .05, .05, .05, .05,
                    .05, .025, .025, .025, .025, .025, .025, .001, .001, .001, .002);

            bool valid = gascomps.IsGasCompositionValid();

            Assert.AreEqual(true, valid);
        }

        [Test]
        public void CalculateCompressibility()
        {
            AGAFlowCalculation.GasComps gascomps =
                new AGAFlowCalculation.GasComps(0, .1, .1, .1, .1, .1, .05, .05, .05, .05,
                    .05, .025, .025, .025, .025, .025, .024, .1, .000, .000, .000);

            AGAFlowCalculation.Aga8 aga8 = new AGAFlowCalculation.Aga8();

            double tempInKelvin = 500;
            double pressureInPSI = 5611.15;

            var agaresult = aga8.CalculateZ(gascomps, tempInKelvin, pressureInPSI);
            double compressibility = agaresult.FlowCompressiblity;
            double specificGravity = agaresult.SpecificGravity;

            bool valid = gascomps.IsGasCompositionValid();
           

            Assert.AreEqual(true, valid);
        }

        [Test]
        public void CheckAGA8SpecificGravity()
        {
            AGAFlowCalculation.Aga8 aga8 = new AGAFlowCalculation.Aga8();

            AGAFlowCalculation.GasComps gascomps = new AGAFlowCalculation.GasComps();
            gascomps.Add(.0268, AGAFlowCalculation.AgaConstants.GasCompositions.Nitrogen_N2);
            gascomps.Add(.0030, AGAFlowCalculation.AgaConstants.GasCompositions.CarbonDioxide_CO2);
            gascomps.Add(.0000, AGAFlowCalculation.AgaConstants.GasCompositions.HydrogenSulfide_H2S);
            gascomps.Add(.6668, AGAFlowCalculation.AgaConstants.GasCompositions.Methane_C1);
            gascomps.Add(.1434, AGAFlowCalculation.AgaConstants.GasCompositions.Ethane_C2);
            gascomps.Add(.1023, AGAFlowCalculation.AgaConstants.GasCompositions.Propane_C3);
            gascomps.Add(.0123, AGAFlowCalculation.AgaConstants.GasCompositions.iButane_iC4);
            gascomps.Add(.0274, AGAFlowCalculation.AgaConstants.GasCompositions.nButane_nC4);
            gascomps.Add(.0000, AGAFlowCalculation.AgaConstants.GasCompositions.iPentane_iC5);
            gascomps.Add(.0000, AGAFlowCalculation.AgaConstants.GasCompositions.nPentane_nC5);
            gascomps.Add(.0180, AGAFlowCalculation.AgaConstants.GasCompositions.nHexane_C6);
            gascomps.Add(.0000, AGAFlowCalculation.AgaConstants.GasCompositions.nHeptane_C7);

            double tempInKelvin = 510;
            double pressureInPSI = 5611.15;

            var agaresult = aga8.CalculateZ(gascomps, tempInKelvin, pressureInPSI);
            double compressibility = agaresult.FlowCompressiblity;
            double specificGravity = agaresult.SpecificGravity;



        }
    }
}
