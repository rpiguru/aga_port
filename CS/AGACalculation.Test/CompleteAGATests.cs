﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Telerik.JustMock;
using AGAFlowCalculation;

namespace AGACalculation.Test
{
    public class CompleteAGATests
    {
        [Test]
        public void AGA_Test_Case_1_Upstream()
        {
            AGAFlowCalculation.GasComps gascomps = new AGAFlowCalculation.GasComps();
            gascomps.Add(.0184, AGAFlowCalculation.AgaConstants.GasCompositions.Nitrogen_N2);
            gascomps.Add(0, AGAFlowCalculation.AgaConstants.GasCompositions.CarbonDioxide_CO2);
            gascomps.Add(.0260, AGAFlowCalculation.AgaConstants.GasCompositions.HydrogenSulfide_H2S);
            gascomps.Add(.7068, AGAFlowCalculation.AgaConstants.GasCompositions.Methane_C1);
            gascomps.Add(.1414, AGAFlowCalculation.AgaConstants.GasCompositions.Ethane_C2);
            gascomps.Add(.0674, AGAFlowCalculation.AgaConstants.GasCompositions.Propane_C3);
            gascomps.Add(.0081, AGAFlowCalculation.AgaConstants.GasCompositions.iButane_iC4);
            gascomps.Add(.0190, AGAFlowCalculation.AgaConstants.GasCompositions.nButane_nC4);
            gascomps.Add(.0038, AGAFlowCalculation.AgaConstants.GasCompositions.iPentane_iC5);
            gascomps.Add(.0043, AGAFlowCalculation.AgaConstants.GasCompositions.nPentane_nC5);
            gascomps.Add(.0026, AGAFlowCalculation.AgaConstants.GasCompositions.nHexane_C6);
            gascomps.Add(.0022, AGAFlowCalculation.AgaConstants.GasCompositions.nHeptane_C7);

            double flowTemperatureInCelcius = 57;
            double pipeReferenceTemperatureInCelcius = 20;
            double orificeReferenceTemperatureInCelcius = 20;
            double baseTemperatureInCelcius = 15;
            double staticPressureKPA = 2818.09;
            double baseStaticPressureKPA = 101.325;
            double differentialPressureKPA = 10.2000;
            double orificeSizeMM = 9.525;
            double pipeSizeMM = 52.370;
            AgaConstants.MaterialType orificeMaterial = AgaConstants.MaterialType.StainlessSteel;
            AgaConstants.MaterialType pipeMaterial = AgaConstants.MaterialType.CarbonSteel;
            bool tapIsUpstream = true;


            AGAFlowCalculation.Aga3 aga3calc = new AGAFlowCalculation.Aga3();
            AGA3Result result = aga3calc.CalculateFlow(flowTemperatureInCelcius, pipeReferenceTemperatureInCelcius,
                orificeReferenceTemperatureInCelcius, baseTemperatureInCelcius, staticPressureKPA,
                baseStaticPressureKPA, differentialPressureKPA, orificeSizeMM, pipeSizeMM,
                orificeMaterial, pipeMaterial, tapIsUpstream, gascomps);


            Assert.AreEqual(2.7478, result.Flow, (2.7478 * .001));
            Assert.AreEqual(.7792, result.SpecificGravity, (.7792 * .001));
            Assert.AreEqual(.9959, result.BaseCompressibility_Zb, (2.7478 * .001));
            Assert.AreEqual(.9280, result.FlowingCompressibility_Zf, (2.7478 * .002));
            //Assert.AreEqual(false, valid);
        }

        [Test]
        public void AGA_Test_Case_1_Downstream()
        {
            AGAFlowCalculation.GasComps gascomps = new AGAFlowCalculation.GasComps();
            gascomps.Add(.0184, AGAFlowCalculation.AgaConstants.GasCompositions.Nitrogen_N2);
            gascomps.Add(0, AGAFlowCalculation.AgaConstants.GasCompositions.CarbonDioxide_CO2);
            gascomps.Add(.0260, AGAFlowCalculation.AgaConstants.GasCompositions.HydrogenSulfide_H2S);
            gascomps.Add(.7068, AGAFlowCalculation.AgaConstants.GasCompositions.Methane_C1);
            gascomps.Add(.1414, AGAFlowCalculation.AgaConstants.GasCompositions.Ethane_C2);
            gascomps.Add(.0674, AGAFlowCalculation.AgaConstants.GasCompositions.Propane_C3);
            gascomps.Add(.0081, AGAFlowCalculation.AgaConstants.GasCompositions.iButane_iC4);
            gascomps.Add(.0190, AGAFlowCalculation.AgaConstants.GasCompositions.nButane_nC4);
            gascomps.Add(.0038, AGAFlowCalculation.AgaConstants.GasCompositions.iPentane_iC5);
            gascomps.Add(.0043, AGAFlowCalculation.AgaConstants.GasCompositions.nPentane_nC5);
            gascomps.Add(.0026, AGAFlowCalculation.AgaConstants.GasCompositions.nHexane_C6);
            gascomps.Add(.0022, AGAFlowCalculation.AgaConstants.GasCompositions.nHeptane_C7);


            double flowTemperatureInCelcius = 57;
            double pipeReferenceTemperatureInCelcius = 20;
            double orificeReferenceTemperatureInCelcius = 20;
            double baseTemperatureInCelcius = 15;
            double staticPressureKPA = 2818.09;
            double baseStaticPressureKPA = 101.325;
            double differentialPressureKPA = 10.2000;
            double orificeSizeMM = 9.525;
            double pipeSizeMM = 52.370;
            AgaConstants.MaterialType orificeMaterial = AgaConstants.MaterialType.StainlessSteel;
            AgaConstants.MaterialType pipeMaterial = AgaConstants.MaterialType.CarbonSteel;
            bool tapIsUpstream = false;


            AGAFlowCalculation.Aga3 aga3calc = new AGAFlowCalculation.Aga3();
            AGA3Result result = aga3calc.CalculateFlow(flowTemperatureInCelcius, pipeReferenceTemperatureInCelcius,
                orificeReferenceTemperatureInCelcius, baseTemperatureInCelcius, staticPressureKPA,
                baseStaticPressureKPA, differentialPressureKPA, orificeSizeMM, pipeSizeMM,
                orificeMaterial, pipeMaterial, tapIsUpstream, gascomps);


            Assert.AreEqual(2.7531, result.Flow, (2.7531 * .001));
            Assert.AreEqual(.7792, result.SpecificGravity, (.7792 * .001));
            Assert.AreEqual(.9959, result.BaseCompressibility_Zb, (.9959 * .001));
            Assert.AreEqual(.9277, result.FlowingCompressibility_Zf, (.9277 * .002));
        }


        [Test]
        public void AGA_Test_Case_2_Upstream()
        {
            AGAFlowCalculation.GasComps gascomps = new AGAFlowCalculation.GasComps();
            gascomps.Add(.0156, AGAFlowCalculation.AgaConstants.GasCompositions.Nitrogen_N2);
            gascomps.Add(.0216, AGAFlowCalculation.AgaConstants.GasCompositions.CarbonDioxide_CO2);
            gascomps.Add(.1166, AGAFlowCalculation.AgaConstants.GasCompositions.HydrogenSulfide_H2S);
            gascomps.Add(.7334, AGAFlowCalculation.AgaConstants.GasCompositions.Methane_C1);
            gascomps.Add(.0697, AGAFlowCalculation.AgaConstants.GasCompositions.Ethane_C2);
            gascomps.Add(.0228, AGAFlowCalculation.AgaConstants.GasCompositions.Propane_C3);
            gascomps.Add(.0044, AGAFlowCalculation.AgaConstants.GasCompositions.iButane_iC4);
            gascomps.Add(.0075, AGAFlowCalculation.AgaConstants.GasCompositions.nButane_nC4);
            gascomps.Add(.0028, AGAFlowCalculation.AgaConstants.GasCompositions.iPentane_iC5);
            gascomps.Add(.0024, AGAFlowCalculation.AgaConstants.GasCompositions.nPentane_nC5);
            gascomps.Add(.0017, AGAFlowCalculation.AgaConstants.GasCompositions.nHexane_C6);
            gascomps.Add(.0015, AGAFlowCalculation.AgaConstants.GasCompositions.nHeptane_C7);

            double flowTemperatureInCelcius = 50;
            double pipeReferenceTemperatureInCelcius = 20;
            double orificeReferenceTemperatureInCelcius = 20;
            double baseTemperatureInCelcius = 15;
            double staticPressureKPA = 9100.94;
            double baseStaticPressureKPA = 101.325;
            double differentialPressureKPA = 11;
            double orificeSizeMM = 47.625;
            double pipeSizeMM = 102.26;
            AgaConstants.MaterialType orificeMaterial = AgaConstants.MaterialType.StainlessSteel;
            AgaConstants.MaterialType pipeMaterial = AgaConstants.MaterialType.CarbonSteel;
            bool tapIsUpstream = true;
            
            AGAFlowCalculation.Aga3 aga3calc = new AGAFlowCalculation.Aga3();
            AGA3Result result = aga3calc.CalculateFlow(flowTemperatureInCelcius, pipeReferenceTemperatureInCelcius,
                orificeReferenceTemperatureInCelcius, baseTemperatureInCelcius, staticPressureKPA,
                baseStaticPressureKPA, differentialPressureKPA, orificeSizeMM, pipeSizeMM,
                orificeMaterial, pipeMaterial, tapIsUpstream, gascomps);

            Assert.AreEqual(146.08, result.Flow, (146.08 * .001));
            Assert.AreEqual(.7456, result.SpecificGravity, (.7456 * .001));
            Assert.AreEqual(.9967, result.BaseCompressibility_Zb, (.9967 * .001));
            Assert.AreEqual(.8098, result.FlowingCompressibility_Zf, (.8098 * .002));
            //Assert.AreEqual(false, valid);
        }

        [Test]
        public void AGA_Test_Case_2_Downstream()
        {
            AGAFlowCalculation.GasComps gascomps = new AGAFlowCalculation.GasComps();
            gascomps.Add(.0156, AGAFlowCalculation.AgaConstants.GasCompositions.Nitrogen_N2);
            gascomps.Add(.0216, AGAFlowCalculation.AgaConstants.GasCompositions.CarbonDioxide_CO2);
            gascomps.Add(.1166, AGAFlowCalculation.AgaConstants.GasCompositions.HydrogenSulfide_H2S);
            gascomps.Add(.7334, AGAFlowCalculation.AgaConstants.GasCompositions.Methane_C1);
            gascomps.Add(.0697, AGAFlowCalculation.AgaConstants.GasCompositions.Ethane_C2);
            gascomps.Add(.0228, AGAFlowCalculation.AgaConstants.GasCompositions.Propane_C3);
            gascomps.Add(.0044, AGAFlowCalculation.AgaConstants.GasCompositions.iButane_iC4);
            gascomps.Add(.0075, AGAFlowCalculation.AgaConstants.GasCompositions.nButane_nC4);
            gascomps.Add(.0028, AGAFlowCalculation.AgaConstants.GasCompositions.iPentane_iC5);
            gascomps.Add(.0024, AGAFlowCalculation.AgaConstants.GasCompositions.nPentane_nC5);
            gascomps.Add(.0017, AGAFlowCalculation.AgaConstants.GasCompositions.nHexane_C6);
            gascomps.Add(.0015, AGAFlowCalculation.AgaConstants.GasCompositions.nHeptane_C7);

            double flowTemperatureInCelcius = 50;
            double pipeReferenceTemperatureInCelcius = 20;
            double orificeReferenceTemperatureInCelcius = 20;
            double baseTemperatureInCelcius = 15;
            double staticPressureKPA = 9100.94;
            double baseStaticPressureKPA = 101.325;
            double differentialPressureKPA = 11;
            double orificeSizeMM = 47.625;
            double pipeSizeMM = 102.26;
            AgaConstants.MaterialType orificeMaterial = AgaConstants.MaterialType.StainlessSteel;
            AgaConstants.MaterialType pipeMaterial = AgaConstants.MaterialType.CarbonSteel;
            bool tapIsUpstream = false;

            AGAFlowCalculation.Aga3 aga3calc = new AGAFlowCalculation.Aga3();
            AGA3Result result = aga3calc.CalculateFlow(flowTemperatureInCelcius, pipeReferenceTemperatureInCelcius,
                orificeReferenceTemperatureInCelcius, baseTemperatureInCelcius, staticPressureKPA,
                baseStaticPressureKPA, differentialPressureKPA, orificeSizeMM, pipeSizeMM,
                orificeMaterial, pipeMaterial, tapIsUpstream, gascomps);

            Assert.AreEqual(146.18, result.Flow, (146.18 * .001));
            Assert.AreEqual(.7456, result.SpecificGravity, (.7456 * .001));
            Assert.AreEqual(.9967, result.BaseCompressibility_Zb, (.9967 * .001));
            Assert.AreEqual(.8097, result.FlowingCompressibility_Zf, (.8097 * .002));
            //Assert.AreEqual(false, valid);
        }


        [Test]
        public void AGA_Test_Case_3_Upstream()
        {
            AGAFlowCalculation.GasComps gascomps = new AGAFlowCalculation.GasComps();
            gascomps.Add(.0500, AGAFlowCalculation.AgaConstants.GasCompositions.Nitrogen_N2);
            gascomps.Add(.1000, AGAFlowCalculation.AgaConstants.GasCompositions.CarbonDioxide_CO2);
            gascomps.Add(.2000, AGAFlowCalculation.AgaConstants.GasCompositions.HydrogenSulfide_H2S);
            gascomps.Add(.6000, AGAFlowCalculation.AgaConstants.GasCompositions.Methane_C1);
            gascomps.Add(.0500, AGAFlowCalculation.AgaConstants.GasCompositions.Ethane_C2);
            gascomps.Add(.0000, AGAFlowCalculation.AgaConstants.GasCompositions.Propane_C3);
            gascomps.Add(0, AGAFlowCalculation.AgaConstants.GasCompositions.iButane_iC4);
            gascomps.Add(0, AGAFlowCalculation.AgaConstants.GasCompositions.nButane_nC4);
            gascomps.Add(0, AGAFlowCalculation.AgaConstants.GasCompositions.iPentane_iC5);
            gascomps.Add(0, AGAFlowCalculation.AgaConstants.GasCompositions.nPentane_nC5);
            gascomps.Add(0, AGAFlowCalculation.AgaConstants.GasCompositions.nHexane_C6);
            gascomps.Add(0, AGAFlowCalculation.AgaConstants.GasCompositions.nHeptane_C7);

            double flowTemperatureInCelcius = 60;
            double pipeReferenceTemperatureInCelcius = 20;
            double orificeReferenceTemperatureInCelcius = 20;
            double baseTemperatureInCelcius = 15;
            double staticPressureKPA = 10342.14;
            double baseStaticPressureKPA = 101.325;
            double differentialPressureKPA = 22.1600;
            double orificeSizeMM = 304.80;
            double pipeSizeMM = 590.55;
            AgaConstants.MaterialType orificeMaterial = AgaConstants.MaterialType.StainlessSteel;
            AgaConstants.MaterialType pipeMaterial = AgaConstants.MaterialType.CarbonSteel;
            bool tapIsUpstream = true;

            AGAFlowCalculation.Aga3 aga3calc = new AGAFlowCalculation.Aga3();
            AGA3Result result = aga3calc.CalculateFlow(flowTemperatureInCelcius, pipeReferenceTemperatureInCelcius,
                orificeReferenceTemperatureInCelcius, baseTemperatureInCelcius, staticPressureKPA,
                baseStaticPressureKPA, differentialPressureKPA, orificeSizeMM, pipeSizeMM,
                orificeMaterial, pipeMaterial, tapIsUpstream, gascomps);


            Assert.AreEqual(8564.77, result.Flow, (8564.77 * .001));
            Assert.AreEqual(.8199, result.SpecificGravity, (.8199 * .001));
            Assert.AreEqual(.9968, result.BaseCompressibility_Zb, (.9968 * .001));
            Assert.AreEqual(.8216, result.FlowingCompressibility_Zf, (.8216 * .002));
            //Assert.AreEqual(false, valid);
        }

        [Test]
        public void AGA_Test_Case_3_Downstream()
        {
            AGAFlowCalculation.GasComps gascomps = new AGAFlowCalculation.GasComps();
            gascomps.Add(.0500, AGAFlowCalculation.AgaConstants.GasCompositions.Nitrogen_N2);
            gascomps.Add(.1000, AGAFlowCalculation.AgaConstants.GasCompositions.CarbonDioxide_CO2);
            gascomps.Add(.2000, AGAFlowCalculation.AgaConstants.GasCompositions.HydrogenSulfide_H2S);
            gascomps.Add(.6000, AGAFlowCalculation.AgaConstants.GasCompositions.Methane_C1);
            gascomps.Add(.0500, AGAFlowCalculation.AgaConstants.GasCompositions.Ethane_C2);
            gascomps.Add(.0000, AGAFlowCalculation.AgaConstants.GasCompositions.Propane_C3);
            gascomps.Add(0, AGAFlowCalculation.AgaConstants.GasCompositions.iButane_iC4);
            gascomps.Add(0, AGAFlowCalculation.AgaConstants.GasCompositions.nButane_nC4);
            gascomps.Add(0, AGAFlowCalculation.AgaConstants.GasCompositions.iPentane_iC5);
            gascomps.Add(0, AGAFlowCalculation.AgaConstants.GasCompositions.nPentane_nC5);
            gascomps.Add(0, AGAFlowCalculation.AgaConstants.GasCompositions.nHexane_C6);
            gascomps.Add(0, AGAFlowCalculation.AgaConstants.GasCompositions.nHeptane_C7);

            double flowTemperatureInCelcius = 60;
            double pipeReferenceTemperatureInCelcius = 20;
            double orificeReferenceTemperatureInCelcius = 20;
            double baseTemperatureInCelcius = 15;
            double staticPressureKPA = 10342.14;
            double baseStaticPressureKPA = 101.325;
            double differentialPressureKPA = 22.1600;
            double orificeSizeMM = 304.80;
            double pipeSizeMM = 590.55;
            AgaConstants.MaterialType orificeMaterial = AgaConstants.MaterialType.StainlessSteel;
            AgaConstants.MaterialType pipeMaterial = AgaConstants.MaterialType.CarbonSteel;
            bool tapIsUpstream = false;

            AGAFlowCalculation.Aga3 aga3calc = new AGAFlowCalculation.Aga3();
            AGA3Result result = aga3calc.CalculateFlow(flowTemperatureInCelcius, pipeReferenceTemperatureInCelcius,
                orificeReferenceTemperatureInCelcius, baseTemperatureInCelcius, staticPressureKPA,
                baseStaticPressureKPA, differentialPressureKPA, orificeSizeMM, pipeSizeMM,
                orificeMaterial, pipeMaterial, tapIsUpstream, gascomps);


            Assert.AreEqual(8575.48, result.Flow, (8575.48 * .001));
            Assert.AreEqual(.8199, result.SpecificGravity, (.8199 * .001));
            Assert.AreEqual(.9968, result.BaseCompressibility_Zb, (.9968 * .001));
            Assert.AreEqual(.8213, result.FlowingCompressibility_Zf, (.8213 * .002));
            //Assert.AreEqual(false, valid);
        }

        [Test]
        public void AGA_Test_Case_4_Upstream()
        {
            AGAFlowCalculation.GasComps gascomps = new AGAFlowCalculation.GasComps();
            gascomps.Add(.0029, AGAFlowCalculation.AgaConstants.GasCompositions.Nitrogen_N2);
            gascomps.Add(.0258, AGAFlowCalculation.AgaConstants.GasCompositions.CarbonDioxide_CO2);
            gascomps.Add(0, AGAFlowCalculation.AgaConstants.GasCompositions.HydrogenSulfide_H2S);
            gascomps.Add(.9709, AGAFlowCalculation.AgaConstants.GasCompositions.Methane_C1);
            gascomps.Add(.0003, AGAFlowCalculation.AgaConstants.GasCompositions.Ethane_C2);
            gascomps.Add(.0001, AGAFlowCalculation.AgaConstants.GasCompositions.Propane_C3);
            gascomps.Add(0, AGAFlowCalculation.AgaConstants.GasCompositions.iButane_iC4);
            gascomps.Add(0, AGAFlowCalculation.AgaConstants.GasCompositions.nButane_nC4);
            gascomps.Add(0, AGAFlowCalculation.AgaConstants.GasCompositions.iPentane_iC5);
            gascomps.Add(0, AGAFlowCalculation.AgaConstants.GasCompositions.nPentane_nC5);
            gascomps.Add(0, AGAFlowCalculation.AgaConstants.GasCompositions.nHexane_C6);
            gascomps.Add(0, AGAFlowCalculation.AgaConstants.GasCompositions.nHeptane_C7);

            double flowTemperatureInCelcius = 22.35;
            double pipeReferenceTemperatureInCelcius = 20;
            double orificeReferenceTemperatureInCelcius = 20;
            double baseTemperatureInCelcius = 15;
            double staticPressureKPA = 9839.99;
            double baseStaticPressureKPA = 101.325;
            double differentialPressureKPA = 6.6130;
            double orificeSizeMM = 88.900;
            double pipeSizeMM = 146.36;
            AgaConstants.MaterialType orificeMaterial = AgaConstants.MaterialType.StainlessSteel;
            AgaConstants.MaterialType pipeMaterial = AgaConstants.MaterialType.CarbonSteel;
            bool tapIsUpstream = true;

            AGAFlowCalculation.Aga3 aga3calc = new AGAFlowCalculation.Aga3();
            AGA3Result result = aga3calc.CalculateFlow(flowTemperatureInCelcius, pipeReferenceTemperatureInCelcius,
                orificeReferenceTemperatureInCelcius, baseTemperatureInCelcius, staticPressureKPA,
                baseStaticPressureKPA, differentialPressureKPA, orificeSizeMM, pipeSizeMM,
                orificeMaterial, pipeMaterial, tapIsUpstream, gascomps);


            Assert.AreEqual(503.44, result.Flow, (503.44 * .001));
            Assert.AreEqual(.5803, result.SpecificGravity, (.5803 * .001));
            Assert.AreEqual(.9980, result.BaseCompressibility_Zb, (.9980 * .001));
            Assert.AreEqual(.8425, result.FlowingCompressibility_Zf, (.8425 * .002));

        }

        [Test]
        public void AGA_Test_Case_4_Downstream()
        {
            AGAFlowCalculation.GasComps gascomps = new AGAFlowCalculation.GasComps();
            gascomps.Add(.0029, AGAFlowCalculation.AgaConstants.GasCompositions.Nitrogen_N2);
            gascomps.Add(.0258, AGAFlowCalculation.AgaConstants.GasCompositions.CarbonDioxide_CO2);
            gascomps.Add(0, AGAFlowCalculation.AgaConstants.GasCompositions.HydrogenSulfide_H2S);
            gascomps.Add(.9709, AGAFlowCalculation.AgaConstants.GasCompositions.Methane_C1);
            gascomps.Add(.0003, AGAFlowCalculation.AgaConstants.GasCompositions.Ethane_C2);
            gascomps.Add(.0001, AGAFlowCalculation.AgaConstants.GasCompositions.Propane_C3);
            gascomps.Add(0, AGAFlowCalculation.AgaConstants.GasCompositions.iButane_iC4);
            gascomps.Add(0, AGAFlowCalculation.AgaConstants.GasCompositions.nButane_nC4);
            gascomps.Add(0, AGAFlowCalculation.AgaConstants.GasCompositions.iPentane_iC5);
            gascomps.Add(0, AGAFlowCalculation.AgaConstants.GasCompositions.nPentane_nC5);
            gascomps.Add(0, AGAFlowCalculation.AgaConstants.GasCompositions.nHexane_C6);
            gascomps.Add(0, AGAFlowCalculation.AgaConstants.GasCompositions.nHeptane_C7);

            double flowTemperatureInCelcius = 22.35;
            double pipeReferenceTemperatureInCelcius = 20;
            double orificeReferenceTemperatureInCelcius = 20;
            double baseTemperatureInCelcius = 15;
            double staticPressureKPA = 9839.99;
            double baseStaticPressureKPA = 101.325;
            double differentialPressureKPA = 6.6130;
            double orificeSizeMM = 88.900;
            double pipeSizeMM = 146.36;
            AgaConstants.MaterialType orificeMaterial = AgaConstants.MaterialType.StainlessSteel;
            AgaConstants.MaterialType pipeMaterial = AgaConstants.MaterialType.CarbonSteel;
            bool tapIsUpstream = false;

            AGAFlowCalculation.Aga3 aga3calc = new AGAFlowCalculation.Aga3();
            AGA3Result result = aga3calc.CalculateFlow(flowTemperatureInCelcius, pipeReferenceTemperatureInCelcius,
                orificeReferenceTemperatureInCelcius, baseTemperatureInCelcius, staticPressureKPA,
                baseStaticPressureKPA, differentialPressureKPA, orificeSizeMM, pipeSizeMM,
                orificeMaterial, pipeMaterial, tapIsUpstream, gascomps);

            Assert.AreEqual(503.63, result.Flow, (503.63 * .001));
            Assert.AreEqual(.5803, result.SpecificGravity, (.5803 * .001));
            Assert.AreEqual(.9980, result.BaseCompressibility_Zb, (.9980 * .001));
            Assert.AreEqual(.8425, result.FlowingCompressibility_Zf, (.8425 * .002));
        }

        [Test]
        public void AGA_Test_Case_5_Upstream()
        {
            AGAFlowCalculation.GasComps gascomps = new AGAFlowCalculation.GasComps();
            gascomps.Add(.0235, AGAFlowCalculation.AgaConstants.GasCompositions.Nitrogen_N2);
            gascomps.Add(.0082, AGAFlowCalculation.AgaConstants.GasCompositions.CarbonDioxide_CO2);
            gascomps.Add(.0021, AGAFlowCalculation.AgaConstants.GasCompositions.HydrogenSulfide_H2S);
            gascomps.Add(.7358, AGAFlowCalculation.AgaConstants.GasCompositions.Methane_C1);
            gascomps.Add(.1296, AGAFlowCalculation.AgaConstants.GasCompositions.Ethane_C2);
            gascomps.Add(.0664, AGAFlowCalculation.AgaConstants.GasCompositions.Propane_C3);
            gascomps.Add(.0088, AGAFlowCalculation.AgaConstants.GasCompositions.iButane_iC4);
            gascomps.Add(.0169, AGAFlowCalculation.AgaConstants.GasCompositions.nButane_nC4);
            gascomps.Add(.0035, AGAFlowCalculation.AgaConstants.GasCompositions.iPentane_iC5);
            gascomps.Add(.0031, AGAFlowCalculation.AgaConstants.GasCompositions.nPentane_nC5);
            gascomps.Add(.0014, AGAFlowCalculation.AgaConstants.GasCompositions.nHexane_C6);
            gascomps.Add(.0007, AGAFlowCalculation.AgaConstants.GasCompositions.nHeptane_C7);

            double flowTemperatureInCelcius = 34;
            double pipeReferenceTemperatureInCelcius = 20;
            double orificeReferenceTemperatureInCelcius = 20;
            double baseTemperatureInCelcius = 15;
            double staticPressureKPA = 2499.9;
            double baseStaticPressureKPA = 101.325;
            double differentialPressureKPA = 75.000;
            double orificeSizeMM = 95.250;
            double pipeSizeMM = 154.05;
            AgaConstants.MaterialType orificeMaterial = AgaConstants.MaterialType.StainlessSteel;
            AgaConstants.MaterialType pipeMaterial = AgaConstants.MaterialType.CarbonSteel;
            bool tapIsUpstream = true;

            AGAFlowCalculation.Aga3 aga3calc = new AGAFlowCalculation.Aga3();
            AGA3Result result = aga3calc.CalculateFlow(flowTemperatureInCelcius, pipeReferenceTemperatureInCelcius,
                orificeReferenceTemperatureInCelcius, baseTemperatureInCelcius, staticPressureKPA,
                baseStaticPressureKPA, differentialPressureKPA, orificeSizeMM, pipeSizeMM,
                orificeMaterial, pipeMaterial, tapIsUpstream, gascomps);

        
            Assert.AreEqual(799.83, result.Flow, (799.83 * .001));
            Assert.AreEqual(.7555, result.SpecificGravity, (.7555 * .001));
            Assert.AreEqual(.9962, result.BaseCompressibility_Zb, (.9962 * .001));
            Assert.AreEqual(.9240, result.FlowingCompressibility_Zf, (.9240 * .002));

        }

        [Test]
        public void AGA_Test_Case_5_Downstream()
        {
            AGAFlowCalculation.GasComps gascomps = new AGAFlowCalculation.GasComps();
            gascomps.Add(.0235, AGAFlowCalculation.AgaConstants.GasCompositions.Nitrogen_N2);
            gascomps.Add(.0082, AGAFlowCalculation.AgaConstants.GasCompositions.CarbonDioxide_CO2);
            gascomps.Add(.0021, AGAFlowCalculation.AgaConstants.GasCompositions.HydrogenSulfide_H2S);
            gascomps.Add(.7358, AGAFlowCalculation.AgaConstants.GasCompositions.Methane_C1);
            gascomps.Add(.1296, AGAFlowCalculation.AgaConstants.GasCompositions.Ethane_C2);
            gascomps.Add(.0664, AGAFlowCalculation.AgaConstants.GasCompositions.Propane_C3);
            gascomps.Add(.0088, AGAFlowCalculation.AgaConstants.GasCompositions.iButane_iC4);
            gascomps.Add(.0169, AGAFlowCalculation.AgaConstants.GasCompositions.nButane_nC4);
            gascomps.Add(.0035, AGAFlowCalculation.AgaConstants.GasCompositions.iPentane_iC5);
            gascomps.Add(.0031, AGAFlowCalculation.AgaConstants.GasCompositions.nPentane_nC5);
            gascomps.Add(.0014, AGAFlowCalculation.AgaConstants.GasCompositions.nHexane_C6);
            gascomps.Add(.0007, AGAFlowCalculation.AgaConstants.GasCompositions.nHeptane_C7);

            double flowTemperatureInCelcius = 34;
            double pipeReferenceTemperatureInCelcius = 20;
            double orificeReferenceTemperatureInCelcius = 20;
            double baseTemperatureInCelcius = 15;
            double staticPressureKPA = 2499.9;
            double baseStaticPressureKPA = 101.325;
            double differentialPressureKPA = 75.000;
            double orificeSizeMM = 95.250;
            double pipeSizeMM = 154.05;
            AgaConstants.MaterialType orificeMaterial = AgaConstants.MaterialType.StainlessSteel;
            AgaConstants.MaterialType pipeMaterial = AgaConstants.MaterialType.CarbonSteel;
            bool tapIsUpstream = false;

            AGAFlowCalculation.Aga3 aga3calc = new AGAFlowCalculation.Aga3();
            AGA3Result result = aga3calc.CalculateFlow(flowTemperatureInCelcius, pipeReferenceTemperatureInCelcius,
                orificeReferenceTemperatureInCelcius, baseTemperatureInCelcius, staticPressureKPA,
                baseStaticPressureKPA, differentialPressureKPA, orificeSizeMM, pipeSizeMM,
                orificeMaterial, pipeMaterial, tapIsUpstream, gascomps);


            Assert.AreEqual(813, result.Flow, (799.83 * .001));
            Assert.AreEqual(.7555, result.SpecificGravity, (.7555 * .001));
            Assert.AreEqual(.9962, result.BaseCompressibility_Zb, (.9962 * .001));
            Assert.AreEqual(.9217, result.FlowingCompressibility_Zf, (.9240 * .002));

        }

        [Test]
        public void AGA_Test_Case_6_Upstream()
        {
            AGAFlowCalculation.GasComps gascomps = new AGAFlowCalculation.GasComps();
            gascomps.Add(.0268, AGAFlowCalculation.AgaConstants.GasCompositions.Nitrogen_N2);
            gascomps.Add(.0030, AGAFlowCalculation.AgaConstants.GasCompositions.CarbonDioxide_CO2);
            gascomps.Add(.0000, AGAFlowCalculation.AgaConstants.GasCompositions.HydrogenSulfide_H2S);
            gascomps.Add(.6668, AGAFlowCalculation.AgaConstants.GasCompositions.Methane_C1);
            gascomps.Add(.1434, AGAFlowCalculation.AgaConstants.GasCompositions.Ethane_C2);
            gascomps.Add(.1023, AGAFlowCalculation.AgaConstants.GasCompositions.Propane_C3);
            gascomps.Add(.0123, AGAFlowCalculation.AgaConstants.GasCompositions.iButane_iC4);
            gascomps.Add(.0274, AGAFlowCalculation.AgaConstants.GasCompositions.nButane_nC4);
            gascomps.Add(.0000, AGAFlowCalculation.AgaConstants.GasCompositions.iPentane_iC5);
            gascomps.Add(.0000, AGAFlowCalculation.AgaConstants.GasCompositions.nPentane_nC5);
            gascomps.Add(.0180, AGAFlowCalculation.AgaConstants.GasCompositions.nHexane_C6);
            gascomps.Add(.0000, AGAFlowCalculation.AgaConstants.GasCompositions.nHeptane_C7);

            double flowTemperatureInCelcius = 7.2;
            double pipeReferenceTemperatureInCelcius = 20;
            double orificeReferenceTemperatureInCelcius = 20;
            double baseTemperatureInCelcius = 15;
            double staticPressureKPA = 2506.33;
            double baseStaticPressureKPA = 101.325;
            double differentialPressureKPA = 17.0500;
            double orificeSizeMM = 19.050;
            double pipeSizeMM = 52.500;
            AgaConstants.MaterialType orificeMaterial = AgaConstants.MaterialType.StainlessSteel;
            AgaConstants.MaterialType pipeMaterial = AgaConstants.MaterialType.CarbonSteel;
            bool tapIsUpstream = true;

            AGAFlowCalculation.Aga3 aga3calc = new AGAFlowCalculation.Aga3();
            AGA3Result result = aga3calc.CalculateFlow(flowTemperatureInCelcius, pipeReferenceTemperatureInCelcius,
                orificeReferenceTemperatureInCelcius, baseTemperatureInCelcius, staticPressureKPA,
                baseStaticPressureKPA, differentialPressureKPA, orificeSizeMM, pipeSizeMM,
                orificeMaterial, pipeMaterial, tapIsUpstream, gascomps);

            Assert.AreEqual(14.687, result.Flow, (14.687 * .001));
            Assert.AreEqual(.8377, result.SpecificGravity, (.8377 * .001));
            Assert.AreEqual(.9951, result.BaseCompressibility_Zb, (.9951 * .001));
            Assert.AreEqual(.8588, result.FlowingCompressibility_Zf, (.8588 * .002));
        }

        [Test]
        public void AGA_Test_Case_6_Downstream()
        {
            AGAFlowCalculation.GasComps gascomps = new AGAFlowCalculation.GasComps();
            gascomps.Add(.0268, AGAFlowCalculation.AgaConstants.GasCompositions.Nitrogen_N2);
            gascomps.Add(.0030, AGAFlowCalculation.AgaConstants.GasCompositions.CarbonDioxide_CO2);
            gascomps.Add(.0000, AGAFlowCalculation.AgaConstants.GasCompositions.HydrogenSulfide_H2S);
            gascomps.Add(.6668, AGAFlowCalculation.AgaConstants.GasCompositions.Methane_C1);
            gascomps.Add(.1434, AGAFlowCalculation.AgaConstants.GasCompositions.Ethane_C2);
            gascomps.Add(.1023, AGAFlowCalculation.AgaConstants.GasCompositions.Propane_C3);
            gascomps.Add(.0123, AGAFlowCalculation.AgaConstants.GasCompositions.iButane_iC4);
            gascomps.Add(.0274, AGAFlowCalculation.AgaConstants.GasCompositions.nButane_nC4);
            gascomps.Add(.0000, AGAFlowCalculation.AgaConstants.GasCompositions.iPentane_iC5);
            gascomps.Add(.0000, AGAFlowCalculation.AgaConstants.GasCompositions.nPentane_nC5);
            gascomps.Add(.0180, AGAFlowCalculation.AgaConstants.GasCompositions.nHexane_C6);
            gascomps.Add(.0000, AGAFlowCalculation.AgaConstants.GasCompositions.nHeptane_C7);

            double flowTemperatureInCelcius = 7.2;
            double pipeReferenceTemperatureInCelcius = 20;
            double orificeReferenceTemperatureInCelcius = 20;
            double baseTemperatureInCelcius = 15;
            double staticPressureKPA = 2506.33;
            double baseStaticPressureKPA = 101.325;
            double differentialPressureKPA = 17.0500;
            double orificeSizeMM = 19.050;
            double pipeSizeMM = 52.500;
            AgaConstants.MaterialType orificeMaterial = AgaConstants.MaterialType.StainlessSteel;
            AgaConstants.MaterialType pipeMaterial = AgaConstants.MaterialType.CarbonSteel;
            bool tapIsUpstream = false;

            AGAFlowCalculation.Aga3 aga3calc = new AGAFlowCalculation.Aga3();
            AGA3Result result = aga3calc.CalculateFlow(flowTemperatureInCelcius, pipeReferenceTemperatureInCelcius,
                orificeReferenceTemperatureInCelcius, baseTemperatureInCelcius, staticPressureKPA,
                baseStaticPressureKPA, differentialPressureKPA, orificeSizeMM, pipeSizeMM,
                orificeMaterial, pipeMaterial, tapIsUpstream, gascomps);

         

            Assert.AreEqual(14.746, result.Flow, (14.687 * .001));
            Assert.AreEqual(.8377, result.SpecificGravity, (.8377 * .001));
            Assert.AreEqual(.9951, result.BaseCompressibility_Zb, (.9951 * .001));
            Assert.AreEqual(.8578, result.FlowingCompressibility_Zf, (.8588 * .002));

        }



        [Test]
        public void AGA_Test_Case_7_Upstream()
        {
            AGAFlowCalculation.GasComps gascomps = new AGAFlowCalculation.GasComps();
            gascomps.Add(.0070, AGAFlowCalculation.AgaConstants.GasCompositions.Nitrogen_N2);
            gascomps.Add(.0400, AGAFlowCalculation.AgaConstants.GasCompositions.CarbonDioxide_CO2);
            gascomps.Add(.0000, AGAFlowCalculation.AgaConstants.GasCompositions.HydrogenSulfide_H2S);
            gascomps.Add(.8720, AGAFlowCalculation.AgaConstants.GasCompositions.Methane_C1);
            gascomps.Add(.0340, AGAFlowCalculation.AgaConstants.GasCompositions.Ethane_C2);
            gascomps.Add(.0250, AGAFlowCalculation.AgaConstants.GasCompositions.Propane_C3);
            gascomps.Add(.0062, AGAFlowCalculation.AgaConstants.GasCompositions.iButane_iC4);
            gascomps.Add(.0090, AGAFlowCalculation.AgaConstants.GasCompositions.nButane_nC4);
            gascomps.Add(.0052, AGAFlowCalculation.AgaConstants.GasCompositions.iPentane_iC5);
            gascomps.Add(.0016, AGAFlowCalculation.AgaConstants.GasCompositions.nPentane_nC5);
            gascomps.Add(.0000, AGAFlowCalculation.AgaConstants.GasCompositions.nHexane_C6);
            gascomps.Add(.0000, AGAFlowCalculation.AgaConstants.GasCompositions.nHeptane_C7);


            double flowTemperatureInCelcius = 1.67;
            double pipeReferenceTemperatureInCelcius = 20;
            double orificeReferenceTemperatureInCelcius = 20;
            double baseTemperatureInCelcius = 15;
            double staticPressureKPA = 299.92;
            double baseStaticPressureKPA = 101.325;
            double differentialPressureKPA = 6.3455;
            double orificeSizeMM = 12.70;
            double pipeSizeMM = 52.500;
            AgaConstants.MaterialType orificeMaterial = AgaConstants.MaterialType.StainlessSteel;
            AgaConstants.MaterialType pipeMaterial = AgaConstants.MaterialType.CarbonSteel;
            bool tapIsUpstream= true;


            AGAFlowCalculation.Aga3 aga3calc = new AGAFlowCalculation.Aga3();
            AGA3Result result = aga3calc.CalculateFlow(flowTemperatureInCelcius, pipeReferenceTemperatureInCelcius,
                orificeReferenceTemperatureInCelcius, baseTemperatureInCelcius, staticPressureKPA,
                baseStaticPressureKPA, differentialPressureKPA, orificeSizeMM, pipeSizeMM,
                orificeMaterial, pipeMaterial, tapIsUpstream, gascomps);


            Assert.AreEqual(1.4335, result.Flow, (1.4335 * .001));
            Assert.AreEqual(.6714, result.SpecificGravity, (.6714 * .001));
            Assert.AreEqual(.9973, result.BaseCompressibility_Zb, (.9973 * .001));
            Assert.AreEqual(.9905, result.FlowingCompressibility_Zf, (.9905 * .002));
        }

        [Test]
        public void AGA_Test_Case_7_Downstream()
        {
            AGAFlowCalculation.GasComps gascomps = new AGAFlowCalculation.GasComps();
            gascomps.Add(.0070, AGAFlowCalculation.AgaConstants.GasCompositions.Nitrogen_N2);
            gascomps.Add(.0400, AGAFlowCalculation.AgaConstants.GasCompositions.CarbonDioxide_CO2);
            gascomps.Add(.0000, AGAFlowCalculation.AgaConstants.GasCompositions.HydrogenSulfide_H2S);
            gascomps.Add(.8720, AGAFlowCalculation.AgaConstants.GasCompositions.Methane_C1);
            gascomps.Add(.0340, AGAFlowCalculation.AgaConstants.GasCompositions.Ethane_C2);
            gascomps.Add(.0250, AGAFlowCalculation.AgaConstants.GasCompositions.Propane_C3);
            gascomps.Add(.0062, AGAFlowCalculation.AgaConstants.GasCompositions.iButane_iC4);
            gascomps.Add(.0090, AGAFlowCalculation.AgaConstants.GasCompositions.nButane_nC4);
            gascomps.Add(.0052, AGAFlowCalculation.AgaConstants.GasCompositions.iPentane_iC5);
            gascomps.Add(.0016, AGAFlowCalculation.AgaConstants.GasCompositions.nPentane_nC5);
            gascomps.Add(.0000, AGAFlowCalculation.AgaConstants.GasCompositions.nHexane_C6);
            gascomps.Add(.0000, AGAFlowCalculation.AgaConstants.GasCompositions.nHeptane_C7);



            double flowTemperatureInCelcius = 1.67;
            double pipeReferenceTemperatureInCelcius = 20;
            double orificeReferenceTemperatureInCelcius = 20;
            double baseTemperatureInCelcius = 15;
            double staticPressureKPA = 299.92;
            double baseStaticPressureKPA = 101.325;
            double differentialPressureKPA = 6.3455;
            double orificeSizeMM = 12.70;
            double pipeSizeMM = 52.500;
            AgaConstants.MaterialType orificeMaterial = AgaConstants.MaterialType.StainlessSteel;
            AgaConstants.MaterialType pipeMaterial = AgaConstants.MaterialType.CarbonSteel;
            bool tapIsUpstream = false;


            AGAFlowCalculation.Aga3 aga3calc = new AGAFlowCalculation.Aga3();
            AGA3Result result = aga3calc.CalculateFlow(flowTemperatureInCelcius, pipeReferenceTemperatureInCelcius,
                orificeReferenceTemperatureInCelcius, baseTemperatureInCelcius, staticPressureKPA,
                baseStaticPressureKPA, differentialPressureKPA, orificeSizeMM, pipeSizeMM,
                orificeMaterial, pipeMaterial, tapIsUpstream, gascomps);


            Assert.AreEqual(1.4489, result.Flow, (1.4489 * .001));
            Assert.AreEqual(.6714, result.SpecificGravity, (.6714 * .001));
            Assert.AreEqual(.9973, result.BaseCompressibility_Zb, (.9973 * .001));
            Assert.AreEqual(.9903, result.FlowingCompressibility_Zf, (.9903 * .002));

        }
    }
}
