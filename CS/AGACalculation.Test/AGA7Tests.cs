﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using Telerik.JustMock;
using AGAFlowCalculation;

namespace AGACalculation.Test
{
    public class CompleteAGA7Tests
    {
        [Test]
        public void AGA_Test_Case_1_Upstream()
        {
            AGAFlowCalculation.GasComps gascomps = new AGAFlowCalculation.GasComps();
            gascomps.Add(.0184, AGAFlowCalculation.AgaConstants.GasCompositions.Nitrogen_N2);
            gascomps.Add(0, AGAFlowCalculation.AgaConstants.GasCompositions.CarbonDioxide_CO2);
            gascomps.Add(.0260, AGAFlowCalculation.AgaConstants.GasCompositions.HydrogenSulfide_H2S);
            gascomps.Add(.7068, AGAFlowCalculation.AgaConstants.GasCompositions.Methane_C1);
            gascomps.Add(.1414, AGAFlowCalculation.AgaConstants.GasCompositions.Ethane_C2);
            gascomps.Add(.0674, AGAFlowCalculation.AgaConstants.GasCompositions.Propane_C3);
            gascomps.Add(.0081, AGAFlowCalculation.AgaConstants.GasCompositions.iButane_iC4);
            gascomps.Add(.0190, AGAFlowCalculation.AgaConstants.GasCompositions.nButane_nC4);
            gascomps.Add(.0038, AGAFlowCalculation.AgaConstants.GasCompositions.iPentane_iC5);
            gascomps.Add(.0043, AGAFlowCalculation.AgaConstants.GasCompositions.nPentane_nC5);
            gascomps.Add(.0026, AGAFlowCalculation.AgaConstants.GasCompositions.nHexane_C6);
            gascomps.Add(.0022, AGAFlowCalculation.AgaConstants.GasCompositions.nHeptane_C7);

            double flowTemperatureInCelcius = 45.5;
            double meterFactor = 2.0000;
            double flowVolume = 10;
            double baseTemperatureInCelcius = 15;
            double staticPressureKPA = 13789.4;
            double baseStaticPressureKPA = 101.325;

 
            AGAFlowCalculation.Aga7 aga7calc = new AGAFlowCalculation.Aga7();
            AGA7Result result = aga7calc.CalculateFlow(flowVolume, flowTemperatureInCelcius,
                baseTemperatureInCelcius, staticPressureKPA, baseStaticPressureKPA, meterFactor, 0.848356, 0.997761, 1);


            Assert.AreEqual(2.894755, result.Flow, (2.894755 * .001));
           // Assert.AreEqual(.7792, result.SpecificGravity, (.7792 * .001));
            //Assert.AreEqual(.9959, result.BaseCompressibility_Zb, (2.7478 * .001));
           // Assert.AreEqual(.9280, result.FlowingCompressibility_Zf, (2.7478 * .002));
        
        }
    }
}
