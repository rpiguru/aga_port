﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UnitConverters
{
    public static class UnitConverter
    {
       

        public static double MCFtoE3M3(double mcf)
        {
            return mcf * 0.028174;
        }

        public static double E3M3toMFC(double e3m3)
        {
            if (e3m3 == 0)
            {
                return 0;
            }
            return e3m3 / 0.028174;
        }



        public static double CELCIUStoFARHENHEIT(double celcius)
        {
            return (celcius * 9 / 5) + 32;
        }

        public static double FARHENHEITtoCELCIUS(double farhenheit)
        {
            return (farhenheit - 32) * 0.55556;
        }



        public static double INCHH2OtoPSI(double inchH2O)
        {
            if (inchH2O == 0)
            {
                return 0;
            }

            return inchH2O / 27.708;
        }

        public static double PSItoINCHH2O(double psi)
        {
            return psi * 27.708;
        }

        public static double INCHH2OToKPA(double inchH2O)
        {
            if (inchH2O == 0)
            {
                return 0;
            }
            else

                return inchH2O * 0.249088908333;
        }

        public static double KPAtoINCHH2O(double kpa)
        {
            return kpa / 0.249088908333;
        }


        public static double PSItoKPA(double psi)
        {
            return (psi * 6.8948);
        }

        public static double KPAtoPSI(double kpa)
        {
            if (kpa == 0)
            {
                return 0;
            }

            return (kpa / 6.8948);
        }


        public static double INCHtoMM(double inch)
        {
            return (inch * 25.4);
           
        }

        public static double MMtoINCH(double mm)
        {
            return (mm * 0.0393700787402);
        }

    }
}
