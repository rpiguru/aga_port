﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AGAFlowCalculation;


namespace AGATester
{
    public partial class Form1 : Form
    {
        private bool userChangedValue;

        public Form1()
        {
            InitializeComponent();
            buttonStart.Click += new EventHandler(buttonStart_Click);

            userChangedValue = false;
            
            List<KeyValuePair<double,string>> pipeMaterialTypes = new List<KeyValuePair<double,string>>();

            pipeMaterialTypes.Add(new KeyValuePair<double, string>(AgaConstants.ThermalExpansionValues.CarbonSteel, "Carbon Steel"));
            pipeMaterialTypes.Add(new KeyValuePair<double, string>(AgaConstants.ThermalExpansionValues.Monel, "Monel"));
            pipeMaterialTypes.Add(new KeyValuePair<double, string>(AgaConstants.ThermalExpansionValues.StainlessSteel, "Stainless Steel"));

            List<KeyValuePair<double, string>> orificeMaterialTypes = new List<KeyValuePair<double, string>>();

            orificeMaterialTypes.Add(new KeyValuePair<double, string>(AgaConstants.ThermalExpansionValues.CarbonSteel, "Carbon Steel"));
            orificeMaterialTypes.Add(new KeyValuePair<double, string>(AgaConstants.ThermalExpansionValues.Monel, "Monel"));
            orificeMaterialTypes.Add(new KeyValuePair<double, string>(AgaConstants.ThermalExpansionValues.StainlessSteel, "Stainless Steel"));

            comboBoxOrificeMaterial.DataSource = orificeMaterialTypes;
            comboBoxOrificeMaterial.DisplayMember = "Value";
            comboBoxOrificeMaterial.ValueMember = "Key";

            comboBoxPipeMaterial.DataSource = pipeMaterialTypes;
            comboBoxPipeMaterial.DisplayMember = "Value";
            comboBoxPipeMaterial.ValueMember = "Key";


            loadSettings();
        }

        void buttonStart_Click(object sender, EventArgs e)
        {
            

           // agaCalculation.AgaCalculation calculation = new agaCalculation.AgaCalculation();

            //double[] gasComps = new double[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }; 

            GasComps gasComps = new GasComps(Convert.ToDouble(numericUpDownMethane.Value) / 100,
                Convert.ToDouble(numericUpDownNitrogen.Value) / 100,
                Convert.ToDouble(numericUpDownCarbonDioxide.Value) / 100,
                Convert.ToDouble(numericUpDownEthane.Value) / 100,
                Convert.ToDouble(numericUpDownPropane.Value) / 100,
                Convert.ToDouble(numericUpDownWater.Value) / 100,
                Convert.ToDouble(numericUpDownHydrogenSulfide.Value) / 100,
                Convert.ToDouble(numericUpDownHydrogen.Value) / 100,
                Convert.ToDouble(numericUpDownCarbonMonoxide.Value) / 100,
                Convert.ToDouble(numericUpDownOxygen.Value) / 100,
                Convert.ToDouble(numericUpDownIButane.Value) / 100,
                Convert.ToDouble(numericUpDownNButane.Value) / 100,
                Convert.ToDouble(numericUpDownIPentane.Value) / 100,
                Convert.ToDouble(numericUpDownNPentane.Value) / 100,
                Convert.ToDouble(numericUpDownNHexane.Value) / 100,
                Convert.ToDouble(numericUpDownNHeptane.Value) / 100,
                Convert.ToDouble(numericUpDownNOctane.Value) / 100,
                Convert.ToDouble(numericUpDownNNonane.Value) / 100,
                Convert.ToDouble(numericUpDownNDecane.Value) / 100,
                Convert.ToDouble(numericUpDownHelium.Value) / 100,
                Convert.ToDouble(numericUpDownArgon.Value) / 100
                );

            AGAFlowCalculation.Aga8 aga8Calcu = new Aga8();

            AGAFlowCalculation.Aga8Result result = aga8Calcu.CalculateZ(gasComps, Convert.ToDouble(numericUpDownFlowingTemp.Value) + 273.15, Convert.ToDouble(numericUpDownStaticPSI.Value));
            
            AGAFlowCalculation.Aga3 aga3Calcu = new Aga3();

            //aga3Calcu.CalculateFlow(gasComps, Convert.ToDouble(numericUpDownFlowingTemp.Value) + 273.15, Convert.ToDouble(numericUpDownOrificeTemperatureC.Value) + 273.15, Convert.ToDouble(numericUpDownOrificeTemperatureC.Value) + 273.15, Convert.ToDouble(numericUpDownRefTemp.Value) + 273.15, Convert.ToDouble(numericUpDownStaticPressure.Value), Convert.ToDouble(numericUpDownRefPress.Value), Convert.ToDouble(numericUpDownDiffPressure.Value), Convert.ToDouble(numericUpDownOrificeDiameter.Value), Convert.ToDouble(numericUpDownTubeDiameter.Value), Convert.ToDouble(comboBoxOrificeMaterial.SelectedValue), Convert.ToDouble(comboBoxPipeMaterial.SelectedValue), checkBoxPressureTapUpstream.Checked);
            

            if (checkBoxUseGasComp.Checked == true)
            {
                // ResultsBO results = new ResultsBO();
                /*
                results = calculation.CalculateFlow(checkBoxPressureTapUpstream.Checked,
                                          true,
                                          (double)comboBoxOrificeMaterial.SelectedValue,
                                          (double)comboBoxPipeMaterial.SelectedValue,
                                          Convert.ToDouble(numericUpDownRefTemp.Value),
                                          Convert.ToDouble(numericUpDownFlowingTemp.Value),
                                          Convert.ToDouble(numericUpDownTubeDiameter.Value),
                                          Convert.ToDouble(numericUpDownOrificeDiameter.Value),
                                          Convert.ToDouble(numericUpDownViscosity.Value),
                                          Convert.ToDouble(numericUpDownStaticPressure.Value),
                                          Convert.ToDouble(numericUpDownDiffPressure.Value),
                                          Convert.ToDouble(1.3),
                                          gasComps, Convert.ToDouble(numericUpDownRefPress.Value));

                numericUpDownResult.Value = Convert.ToDecimal(results.Flow / 1000);
                numericUpDownFlowMCF.Value = Convert.ToDecimal(UnitConverters.UnitConverter.E3M3toMFC(Convert.ToDouble(numericUpDownResult.Value)));
                numericUpDownBaseCompressibility.Value = Convert.ToDecimal(results.ZBase);
                numericUpDownFlowingCompressibility.Value = Convert.ToDecimal(results.ZFlow);
                numericUpDownSpecificGravity.Value = Convert.ToDecimal(results.SpecificGravity);
                 */                
            }
            else
            {/*
                numericUpDownResult.Value = Convert.ToDecimal(calculation.CalculateFlow(checkBoxPressureTapUpstream.Checked,
                                        true,
                                        (double)comboBoxOrificeMaterial.SelectedValue,
                                        (double)comboBoxPipeMaterial.SelectedValue,
                                        Convert.ToDouble(numericUpDownRefTemp.Value),
                                        Convert.ToDouble(numericUpDownFlowingTemp.Value),
                                        Convert.ToDouble(numericUpDownTubeDiameter.Value),
                                        Convert.ToDouble(numericUpDownOrificeDiameter.Value),
                                        Convert.ToDouble(numericUpDownViscosity.Value),
                                        Convert.ToDouble(numericUpDownStaticPressure.Value),
                                        Convert.ToDouble(numericUpDownDiffPressure.Value),
                                        Convert.ToDouble(1.3),
                                        Convert.ToDouble(numericUpDownSpecificGravity.Value),
                                        Convert.ToDouble(numericUpDownFlowingCompressibility.Value),
                                        Convert.ToDouble(numericUpDownBaseCompressibility.Value),
                                        Convert.ToDouble(numericUpDownRefPress.Value))/1000);

                numericUpDownFlowMCF.Value = Convert.ToDecimal(UnitConverters.UnitConverter.E3M3toMFC(Convert.ToDouble(numericUpDownResult.Value)));
              */
            }

        }

      

        private void buttonCalculate_Click(object sender, EventArgs e)
        {
            GasComps gasComps = new GasComps(Convert.ToDouble(numericUpDownMethane.Value) / 100,
                Convert.ToDouble(numericUpDownNitrogen.Value) / 100,
                Convert.ToDouble(numericUpDownCarbonDioxide.Value) / 100,
                Convert.ToDouble(numericUpDownEthane.Value) / 100,
                Convert.ToDouble(numericUpDownPropane.Value) / 100,
                Convert.ToDouble(numericUpDownWater.Value) / 100,
                Convert.ToDouble(numericUpDownHydrogenSulfide.Value) / 100,
                Convert.ToDouble(numericUpDownHydrogen.Value) / 100,
                Convert.ToDouble(numericUpDownCarbonMonoxide.Value) / 100,
                Convert.ToDouble(numericUpDownOxygen.Value) / 100,
                Convert.ToDouble(numericUpDownIButane.Value) / 100,
                Convert.ToDouble(numericUpDownNButane.Value) / 100,
                Convert.ToDouble(numericUpDownIPentane.Value) / 100,
                Convert.ToDouble(numericUpDownNPentane.Value) / 100,
                Convert.ToDouble(numericUpDownNHexane.Value) / 100,
                Convert.ToDouble(numericUpDownNHeptane.Value) / 100,
                Convert.ToDouble(numericUpDownNOctane.Value) / 100,
                Convert.ToDouble(numericUpDownNNonane.Value) / 100,
                Convert.ToDouble(numericUpDownNDecane.Value) / 100,
                Convert.ToDouble(numericUpDownHelium.Value) / 100,
                Convert.ToDouble(numericUpDownArgon.Value) / 100
                );

            AGAFlowCalculation.Aga8 aga8Calcu = new Aga8();
            Aga8Result result = aga8Calcu.CalculateZ(gasComps, Convert.ToDouble(numericUpDownFlowingTemp.Value) + 273.15, Convert.ToDouble(numericUpDownStaticPSI.Value));

            //aga3Calculation.Aga8Calculation calculate = new aga3Calculation.Aga8Calculation();
           
           // double massDensity = calculate.CalculateZ(gasComps, Convert.ToDouble(numericUpDownFlowingTemp.Value), Convert.ToDouble(numericUpDownStaticPressure.Value));

          //  numericUpDownAga8Result.Value = Convert.ToDecimal(massDensity);
        }

   

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void numericUpDownStaticPressure_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (userChangedValue == false)
                {
                    userChangedValue = true;
                    numericUpDownStaticPSI.Value = Convert.ToDecimal(UnitConverters.UnitConverter.KPAtoPSI(Convert.ToDouble(numericUpDownStaticPressure.Value)));
                }
                userChangedValue = false;
            }
            catch
            {

            }
        }

        private void numericUpDownStaticPSI_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (userChangedValue == false)
                {
                    userChangedValue = true;
                    numericUpDownStaticPressure.Value = Convert.ToDecimal(UnitConverters.UnitConverter.PSItoKPA(Convert.ToDouble(numericUpDownStaticPSI.Value)));
                }
                userChangedValue = false;
            }
            catch
            {

            }
        }

        private void numericUpDownDiffPressure_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (userChangedValue == false)
                {
                    userChangedValue = true;
                    numericUpDownDiffInH2O.Value = Convert.ToDecimal(UnitConverters.UnitConverter.KPAtoINCHH2O(Convert.ToDouble(numericUpDownDiffPressure.Value)));
                }
                userChangedValue = false;
            }
            catch
            {

            }
        }

        private void numericUpDownDiffInH2O_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (userChangedValue == false)
                {
                    userChangedValue = true;
                    numericUpDownDiffPressure.Value = Convert.ToDecimal(UnitConverters.UnitConverter.INCHH2OToKPA(Convert.ToDouble(numericUpDownDiffInH2O.Value)));
                }
                userChangedValue = false;
            }
            catch
            {

            }
        }

        private void numericUpDownFlowingTemp_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (userChangedValue == false)
                {
                    userChangedValue = true;
                    numericUpDownTemperatureF.Value = Convert.ToDecimal(UnitConverters.UnitConverter.CELCIUStoFARHENHEIT(Convert.ToDouble(numericUpDownFlowingTemp.Value)));
                }
                userChangedValue = false;
            }
            catch
            {

            }
        }

        private void numericUpDownTemperatureF_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (userChangedValue == false)
                {
                    userChangedValue = true;
                    numericUpDownFlowingTemp.Value = Convert.ToDecimal(UnitConverters.UnitConverter.FARHENHEITtoCELCIUS(Convert.ToDouble(numericUpDownTemperatureF.Value)));
                }
                userChangedValue = false;
            }
            catch
            {

            }
        }

        private void numericUpDownOrificeDiameter_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (userChangedValue == false)
                {
                    userChangedValue = true;
                    numericUpDownOrificeInch.Value = Convert.ToDecimal(UnitConverters.UnitConverter.MMtoINCH(Convert.ToDouble(numericUpDownOrificeDiameter.Value)));
                }
                userChangedValue = false;
            }
            catch
            {

            }
        }

        private void numericUpDownOrificeInch_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (userChangedValue == false)
                {
                    userChangedValue = true;
                    numericUpDownOrificeDiameter.Value = Convert.ToDecimal(UnitConverters.UnitConverter.INCHtoMM(Convert.ToDouble(numericUpDownOrificeInch.Value)));
                }
                userChangedValue = false;
            }
            catch
            {
                userChangedValue = false;
            }
        }

        private void numericUpDownTubeDiameter_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (userChangedValue == false)
                {
                    userChangedValue = true;
                    numericUpDownPipeInch.Value = Convert.ToDecimal(UnitConverters.UnitConverter.MMtoINCH(Convert.ToDouble(numericUpDownTubeDiameter.Value)));
                }
                userChangedValue = false;
            }
            catch
            {
                userChangedValue = false;
            }
        }

        private void numericUpDownPipeInch_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (userChangedValue == false)
                {
                    userChangedValue = true;
                    numericUpDownTubeDiameter.Value = Convert.ToDecimal(UnitConverters.UnitConverter.INCHtoMM(Convert.ToDouble(numericUpDownPipeInch.Value)));
                }
                userChangedValue = false;
            }
            catch
            {
                userChangedValue = false;
            }
        }

        private void checkBoxUseGasComp_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxUseGasComp.Checked == true)
            {
                numericUpDownSpecificGravity.Enabled = false;
                numericUpDownBaseCompressibility.Enabled = false;
                numericUpDownFlowingCompressibility.Enabled = false;
            }
            else
            {
                numericUpDownSpecificGravity.Enabled = true;
                numericUpDownBaseCompressibility.Enabled = true;
                numericUpDownFlowingCompressibility.Enabled = true;
            }
        }

       

        private void numericUpDown3_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label62_Click(object sender, EventArgs e)
        {

        }

        private void numericUpDownAtmosPressKPA_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (userChangedValue == false)
                {
                    userChangedValue = true;
                    numericUpDownAtmosPressPSI.Value = Convert.ToDecimal(UnitConverters.UnitConverter.KPAtoPSI(Convert.ToDouble(numericUpDownAtmosPressPSI.Value)));
                }
                userChangedValue = false;
            }
            catch
            {
                userChangedValue = false;
            }
        }

        private void numericUpDownAtmosPressPSI_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (userChangedValue == false)
                {
                    userChangedValue = true;
                    numericUpDownAtmosPressKPA.Value = Convert.ToDecimal(UnitConverters.UnitConverter.PSItoKPA(Convert.ToDouble(numericUpDownAtmosPressKPA.Value)));
                }
                userChangedValue = false;
            }
            catch
            {
                userChangedValue = false;
            }
        }

        private void numericUpDownRefPress_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (userChangedValue == false)
                {
                    userChangedValue = true;
                    numericUpDownRefPressPSI.Value = Convert.ToDecimal(UnitConverters.UnitConverter.KPAtoPSI(Convert.ToDouble(numericUpDownRefPress.Value)));
                }
                userChangedValue = false;
            }
            catch
            {
                userChangedValue = false;
            }
        }

        private void numericUpDownRefPressPSI_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (userChangedValue == false)
                {
                    userChangedValue = true;
                    numericUpDownRefPress.Value = Convert.ToDecimal(UnitConverters.UnitConverter.PSItoKPA(Convert.ToDouble(numericUpDownRefPressPSI.Value)));
                }
                userChangedValue = false;
            }
            catch
            {
                userChangedValue = false;
            }
        }

        private void numericUpDownRefTemp_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (userChangedValue == false)
                {
                    userChangedValue = true;
                    numericUpDownBaseTempF.Value = Convert.ToDecimal(UnitConverters.UnitConverter.CELCIUStoFARHENHEIT(Convert.ToDouble(numericUpDownRefTemp.Value)));
                }
                userChangedValue = false;
            }
            catch
            {
                userChangedValue = false;
            }
        }

        private void numericUpDownBaseTempF_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (userChangedValue == false)
                {
                    userChangedValue = true;
                    numericUpDownRefTemp.Value = Convert.ToDecimal(UnitConverters.UnitConverter.FARHENHEITtoCELCIUS(Convert.ToDouble(numericUpDownBaseTempF.Value)));
                }
                userChangedValue = false;
            }
            catch
            {
                userChangedValue = false;
            }
        }

        private void numericUpDownOrificeTemperatureC_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (userChangedValue == false)
                {
                    userChangedValue = true;
                    numericUpDownOrificeTemperatureF.Value = Convert.ToDecimal(UnitConverters.UnitConverter.CELCIUStoFARHENHEIT(Convert.ToDouble(numericUpDownOrificeTemperatureC.Value)));
                }
                userChangedValue = false;
            }
            catch
            {
                userChangedValue = false;
            }
        }

        private void numericUpDownOrificeTemperatureF_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (userChangedValue == false)
                {
                    userChangedValue = true;
                    numericUpDownOrificeTemperatureC.Value = Convert.ToDecimal(UnitConverters.UnitConverter.FARHENHEITtoCELCIUS(Convert.ToDouble(numericUpDownOrificeTemperatureF.Value)));
                }
                userChangedValue = false;
            }
            catch
            {
                userChangedValue = false;     
            }
        }

        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (userChangedValue == false)
                {
                    userChangedValue = true;
                    numericUpDownPipeTemperatureF.Value = Convert.ToDecimal(UnitConverters.UnitConverter.CELCIUStoFARHENHEIT(Convert.ToDouble(numericUpDownPipeTemperatureC.Value)));
                }
                userChangedValue = false;
            }
            catch
            {
                userChangedValue = false;
            }
        }

        private void numericUpDownPipeTemperatureF_ValueChanged(object sender, EventArgs e)
        {
            try
            {
                if (userChangedValue == false)
                {
                    userChangedValue = true;
                    numericUpDownPipeTemperatureC.Value = Convert.ToDecimal(UnitConverters.UnitConverter.FARHENHEITtoCELCIUS(Convert.ToDouble(numericUpDownPipeTemperatureF.Value)));
                }
                userChangedValue = false;
            }
            catch
            {
                userChangedValue = false;
            }
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void saveAsDefaultSettingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Settings1.Default.AtmosphericPressure = numericUpDownAtmosPressKPA.Value;
            Settings1.Default.BaseCompress = numericUpDownBaseCompressibility.Value;
            Settings1.Default.BasePress = numericUpDownRefPress.Value;
            Settings1.Default.BaseTemp = numericUpDownRefTemp.Value;
            Settings1.Default.CarbonDioxide = numericUpDownCarbonDioxide.Value;
            Settings1.Default.CarbonMonoxide = numericUpDownCarbonMonoxide.Value;
            Settings1.Default.DiffPressure = numericUpDownDiffPressure.Value;
            Settings1.Default.Ethane = numericUpDownEthane.Value;
            Settings1.Default.FlowingCompress =numericUpDownFlowingCompressibility.Value;
            Settings1.Default.FlowingTemp = numericUpDownFlowingTemp.Value;
            Settings1.Default.GasCompsEnabled = checkBoxUseGasComp.Checked;
            Settings1.Default.Helium = numericUpDownHelium.Value;
            Settings1.Default.Hydrogen = numericUpDownHydrogen.Value;
            Settings1.Default.HydrogenSulfide = numericUpDownHydrogenSulfide.Value;
            Settings1.Default.iButane = numericUpDownIButane.Value;
            Settings1.Default.IdealSpecGrav = numericUpDownSpecificGravity.Value;
            Settings1.Default.iPentane = numericUpDownIPentane.Value;
            Settings1.Default.Methane = numericUpDownMethane.Value;
            Settings1.Default.nButane = numericUpDownNButane.Value;
            Settings1.Default.nDecane = numericUpDownNDecane.Value;
            Settings1.Default.nHeptane = numericUpDownNHeptane.Value;
            Settings1.Default.nHexane = numericUpDownNHexane.Value;
            Settings1.Default.Nitrogen = numericUpDownNitrogen.Value;
            Settings1.Default.nNonane = numericUpDownNNonane.Value;
            Settings1.Default.nOctane = numericUpDownNOctane.Value;
            Settings1.Default.nPentane = numericUpDownNPentane.Value;
            Settings1.Default.OrificeDiameter = numericUpDownOrificeDiameter.Value;
            Settings1.Default.OrificeMaterial = comboBoxOrificeMaterial.SelectedIndex;
            Settings1.Default.OrificeTemp = numericUpDownOrificeTemperatureC.Value;
            Settings1.Default.Oxygen = numericUpDownOxygen.Value;
            Settings1.Default.PipeDiameter = numericUpDownTubeDiameter.Value;
            Settings1.Default.PipeMaterial = comboBoxPipeMaterial.SelectedIndex;
            Settings1.Default.PipeTemp = numericUpDownPipeTemperatureC.Value;
            Settings1.Default.PressureTapUpStream = checkBoxPressureTapUpstream.Checked;
            Settings1.Default.Propane = numericUpDownPropane.Value;
            Settings1.Default.StaticPressure = numericUpDownStaticPressure.Value;
            Settings1.Default.Water = numericUpDownWater.Value;

            Settings1.Default.Save();

        }

        private void loadSettings()
        {
            numericUpDownAtmosPressKPA.Value = Settings1.Default.AtmosphericPressure;
            numericUpDownBaseCompressibility.Value = Settings1.Default.BaseCompress;
            numericUpDownRefPress.Value = Settings1.Default.BasePress;
            numericUpDownRefTemp.Value = Settings1.Default.BaseTemp;
            numericUpDownCarbonDioxide.Value = Settings1.Default.CarbonDioxide;
            numericUpDownCarbonMonoxide.Value = Settings1.Default.CarbonMonoxide;
            numericUpDownDiffPressure.Value = Settings1.Default.DiffPressure;
            numericUpDownEthane.Value = Settings1.Default.Ethane;
            numericUpDownFlowingCompressibility.Value = Settings1.Default.FlowingCompress;
            numericUpDownFlowingTemp.Value = Settings1.Default.FlowingTemp;
            checkBoxUseGasComp.Checked = Settings1.Default.GasCompsEnabled;
            numericUpDownHelium.Value = Settings1.Default.Helium;
            numericUpDownHydrogen.Value = Settings1.Default.Hydrogen;
            numericUpDownHydrogenSulfide.Value = Settings1.Default.HydrogenSulfide;
            numericUpDownIButane.Value = Settings1.Default.iButane;
            numericUpDownSpecificGravity.Value = Settings1.Default.IdealSpecGrav;
            numericUpDownIPentane.Value = Settings1.Default.iPentane;
            numericUpDownMethane.Value = Settings1.Default.Methane;
            numericUpDownNButane.Value = Settings1.Default.nButane;
            numericUpDownNDecane.Value = Settings1.Default.nDecane;
            numericUpDownNHeptane.Value = Settings1.Default.nHeptane;
            numericUpDownNHexane.Value = Settings1.Default.nHexane;
            numericUpDownNitrogen.Value = Settings1.Default.Nitrogen;
            numericUpDownNNonane.Value = Settings1.Default.nNonane;
            numericUpDownNOctane.Value = Settings1.Default.nOctane;
            numericUpDownNPentane.Value = Settings1.Default.nPentane;
            numericUpDownOrificeDiameter.Value = Settings1.Default.OrificeDiameter;
            comboBoxOrificeMaterial.SelectedIndex = Settings1.Default.OrificeMaterial;
            numericUpDownOrificeTemperatureC.Value = Settings1.Default.OrificeTemp;
            numericUpDownOxygen.Value = Settings1.Default.Oxygen;
            numericUpDownTubeDiameter.Value = Settings1.Default.PipeDiameter;
            comboBoxPipeMaterial.SelectedIndex = Settings1.Default.PipeMaterial;
            numericUpDownPipeTemperatureC.Value = Settings1.Default.PipeTemp;
            checkBoxPressureTapUpstream.Checked = Settings1.Default.PressureTapUpStream;
            numericUpDownPropane.Value = Settings1.Default.Propane;
            numericUpDownStaticPressure.Value = Settings1.Default.StaticPressure;
            numericUpDownWater.Value = Settings1.Default.Water;
        }

        private void groupBox3_Enter(object sender, EventArgs e)
        {

        }

        private void buttonStart_Click_1(object sender, EventArgs e)
        {
            GasComps gasComps = new GasComps(
                90.6724 / 100,
                3.1284 / 100,
                0.4676 / 100,
                4.5279 / 100,
                0.828 / 100,
                0,
                0,
                0,
                0,
                0,
                0.1037 / 100,
                0.1563 / 100,
                0.0321 / 100,
                0.0443 / 100,
                0.0393 / 100,
                0,
                0,
                0,
                0,
                0.01,
                0
                );

            AGAFlowCalculation.Aga8 aga8Calcu = new Aga8();
            Aga8Result result = aga8Calcu.CalculateZ(gasComps, Convert.ToDouble(numericUpDownFlowingTemp.Value) + 273.15, Convert.ToDouble(numericUpDownStaticPSI.Value));

        }

    }
}
