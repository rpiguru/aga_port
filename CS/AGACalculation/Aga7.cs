﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AGAFlowCalculation
{

    public struct AGA7Result
    {
        public double Flow;
        public double SpecificGravity;
        public double FlowingCompressibility_Zf;
        public double BaseCompressibility_Zb;
        public double Compressibility_FPV;
    }

    public class Aga7
    {
        
/// <summary>
/// Calculates AGA3 Flow based using gas compositions and AGA8 calcuations
/// Pressure units are in KPA
/// Temperature units are in Celcius
/// Length units are in mm
/// </summary>
 /// <param name="flowVolumeInM3">Flow Volume (m3)</param>
/// <param name="flowTemperatureInCelcius">Flowing Temperature (C)</param>
/// <param name="baseTemperatureInCelcius">Base Temperature (C)</param>
/// <param name="staticPressureKPA">Static Pressure (KPA)</param>
/// <param name="baseStaticPressureKPA">Base Static Pressure (KPA)</param>
/// <param name="differentialPressureKPA">Differential Pressure (KPA)</param>
        /// <param name="meterFactor">Meter Factor</param>
        /// <param name="gasCompositions">Gas Composition Collection</param>
/// <returns></returns>
        public AGA7Result CalculateFlow(double flowVolume, double flowTemperature, double baseTemperature, double staticPressure,
            double basePressure,  double meterFactor, GasComps gasCompositions)
        {
         
            double staticPressBar = UnitConverter.KPAToBar(staticPressure);
         
            Aga8 ZCalc = new Aga8();
            Aga8Result calcZResult = ZCalc.CalculateZ(gasCompositions, UnitConverter.CELCIUStoKELVIN(flowTemperature), UnitConverter.KPAtoPSI(UnitConverter.BarToKPA(staticPressBar)));

            double FlowZCalc = calcZResult.FlowCompressiblity;
            double BaseZCalc = ZCalc.CalculateZ(gasCompositions, UnitConverter.CELCIUStoKELVIN(baseTemperature), UnitConverter.KPAtoPSI(basePressure)).FlowCompressiblity;
            double SpecificGravity = Math.Round(calcZResult.SpecificGravity, 5);

            AGA7Result aga7 =  CalculateFlow(flowVolume, UnitConverter.CELCIUStoKELVIN(flowTemperature),
                UnitConverter.CELCIUStoKELVIN(baseTemperature), staticPressure, basePressure, meterFactor,
                FlowZCalc, BaseZCalc, SpecificGravity);

            return aga7;
        }


  /// <summary>
/// Calculates AGA3 Flow based pre-calculated compressibility and specific gravity
/// Pressure units are in KPA
/// Temperature units are in Celcius
/// Length units are in mm
/// </summary>
        /// <param name="flowVolumeInM3">Flow Volume (m3)</param>
        /// <param name="flowTemperatureInCelcius">Flowing Temperature (C)</param>
/// <param name="baseTemperatureInCelcius">Base Temperature (C)</param>
/// <param name="staticPressureKPA">Static Pressure (KPA)</param>
/// <param name="baseStaticPressureKPA">Base Static Pressure (KPA)</param>
/// <param name="differentialPressureKPA">Differential Pressure (KPA)</param>
        /// <param name="meterFactor">Meter Factor</param>
/// <param name="flowCompressibility">Compressibility factor at flowing conditions</param>
/// <param name="baseCompressibility">Compressibility factor at base conditions</param>
/// <param name="specificGravity">Specific Gravity (Ideal gas relative density)</param>
  /// <returns></returns>
        public AGA7Result CalculateFlow(double flowVolume, double flowTemperature,double baseTemperature, double staticPressure, 
            double basePressure,double meterFactor,
            double flowCompressibility, double baseCompressibility, double specificGravity)
        {

            return CalculateAGA7(flowVolume, UnitConverter.CELCIUStoKELVIN(flowTemperature),
             UnitConverter.CELCIUStoKELVIN(baseTemperature), staticPressure, basePressure, 
             meterFactor, flowCompressibility, baseCompressibility, specificGravity);
        }

        /// <summary>
        /// Pressure units are in KPA
        /// Temperature units are in Kelvin
        /// Length units are in mm
        /// </summary>
        /// <param name="flowVolumeInM3">Flow Volume (m3)</param>
        /// <param name="flowTemperatureInKelvin"></param>
        /// <param name="baseTemperatureInKelvin"></param>
        /// <param name="staticPressureKPA"></param>
        /// <param name="basePressureKPA"></param>
        /// <param name="meterFactor">Meter Factor</param>
        /// <param name="flowCompressibility">Compressibility factor at flowing conditions</param>
        /// <param name="baseCompressibility">Compressibility factor at base conditions</param>
        /// <param name="specificGravity">Specific Gravity (Ideal gas relative density)</param>
        /// <returns></returns>
        private AGA7Result CalculateAGA7(double flowVolumeInM3, double flowTemperatureInKelvin,
            double baseTemperatureInKelvin, double staticPressureKPA, double basePressureKPA, 
             double meterFactor, double FlowZCalc, double BaseZCalc, double SpecificGravity)
        {
            int decimalPlaces = 10;

#if DEMO
            decimalPlaces = 0;
#endif


            AGA7Result results = new AGA7Result();

            
            results.Compressibility_FPV = Math.Round(Math.Sqrt(BaseZCalc / FlowZCalc),decimalPlaces);// Math.Round(Math.Sqrt(BaseZCalc / FlowZCalc), 0);
            results.SpecificGravity = Math.Round(SpecificGravity,decimalPlaces);
            results.BaseCompressibility_Zb = Math.Round(BaseZCalc,decimalPlaces);
            results.FlowingCompressibility_Zf = Math.Round(FlowZCalc, decimalPlaces);


            double fpm = staticPressureKPA / 101.56;  //101.56??
            double fpb = 101.56/basePressureKPA;
            double ftm = (15.55556 + 273.15) / flowTemperatureInKelvin;
            double ftb = baseTemperatureInKelvin / (15.55556 + 273.15);
            double s = BaseZCalc / FlowZCalc;
            double BMV = fpm * fpb * ftm * ftb * s;

            double BMV_MeterFactor = BMV * meterFactor;

            results.Flow = Math.Round((flowVolumeInM3 * BMV_MeterFactor) / 1000, decimalPlaces);
            
            
            return results;
        }
    }
}
