﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace AGAFlowCalculation
{
    public struct Aga8Result
    {
        public double FlowCompressiblity;
        public double SpecificGravity;
    }
    
    public class Aga8
    {
        static void Main(string[] args)
        {
            Aga8 aga = new Aga8();

            GasComps g = new GasComps();
            aga.CalculateZ(g, 293.15, 14.5038);

        }

         const double RGAS = 0.00831451;

         double EPSP, EPSR, EPSMIN, RHO = 0, RHOL = 0, RHOH = 0, PRHOL = 0, PRHOH = 0;
        
         double UU;
         double RK3PO;
         double Q2PO;
         double BMIX;

         double CODE;

         double RK5PO;
         double RK2P5;
         double U5PO;
         double U2P5;
         double WW;
         double Q1PO;
         double HH;

         double MWX;

         double[] B;
         double[] FN;
         

         double TOLD;


         double[] gasComps = new double[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 }; 

        /// </summary>
        /// <param name="gasComps"></param>
        /// <param name="temperatureInKelvin"></param>
        /// <param name="pressurePSI"></param>
        /// <returns></returns>
        public Aga8Result CalculateZ(GasComps gasCompositions, double temperatureInKelvin, double pressurePSI)
        {

            //if (gasCompositions == null)
            //{
            //    throw new ArgumentNullException("gasComps must have a value");
            //}

            //if (!gasCompositions.IsGasCompositionValid())
            //{
            //    throw new ArgumentException("Total Gas Composition Does not add up to 100%. Current Total:" + Math.Round(gasCompositions.TotalComposition(), 4));
            //}
            


            double TMFRAK, RK5PO, RK2P5, U5PO, U2P5, Q1PO;
            double XIJ, EIJ, WIJ, EOP5, E2PO, E3PO, E3P5, E4P5, E6PO;
            double E7P5, E9P5, E12PO, E12P5;
            double E11PO, S3;

            //this.gasComps = gasCompositions.GasCompositionArray;
            this.gasComps = new double[] { 90.6724 / 100,
                3.1284 / 100,
                0.4676 / 100,
                4.5279 / 100,
                0.828 / 100,
                0,
                0,
                0,
                0,
                0,
                0.1037 / 100,
                0.1563 / 100,
                0.0321 / 100,
                0.0443 / 100,
                0.0393 / 100,
                0,
                0,
                0,
                0,
                0.01 / 100,
                0 
            }; 

            TMFRAK = 0;

            for (int i = 0; i < gasComps.Length; i++)
            {
                TMFRAK = TMFRAK + gasComps[i];
            }

            B = new double[18];
            for (int i = 0; i < 18; i++)
            {
                B[i] = 0;
            }

            RK5PO = 0;
            RK2P5 = 0;
            U5PO = 0;
            U2P5 = 0;
            WW = 0;
            Q1PO = 0;
            HH = 0;

            MWX = 0;

            for (int i = 0; i < gasComps.Length; i++)
            {
                MWX = MWX + gasComps[i] * AgaConstants.MolarMass[i];
            }

            for (int i = 0; i < gasComps.Length; i++)
            {
                RK2P5 = RK2P5 + gasComps[i] * AgaConstants.K_Size[i] * AgaConstants.K_Size[i] * Math.Sqrt(AgaConstants.K_Size[i]);
                U2P5 = U2P5 + gasComps[i] * AgaConstants.E_Energy[i] * AgaConstants.E_Energy[i] * Math.Sqrt(AgaConstants.E_Energy[i]);
                HH = HH + gasComps[i] * AgaConstants.W_Association[i];
                Q1PO = Q1PO + gasComps[i] * AgaConstants.Q_Quadrupole[i];
                WW = WW +gasComps[i] * AgaConstants.G_Orientation[i];

                for (int j = i; j < gasComps.Length; j++)
                { 
                    XIJ = 0;

                    if (i != j)
                    {
                        XIJ = 2.0 * gasComps[i] * gasComps[j];
                    }
                    else
                    {
                        XIJ = gasComps[i] * gasComps[j];
                    }

                    if (AgaConstants.K_BinaryInteration[i][j] != 1)
                    {
                        RK5PO = RK5PO + XIJ*(Math.Pow(AgaConstants.K_BinaryInteration[i][j],5)-1) * Math.Sqrt(Math.Pow(AgaConstants.K_Size[i],5)*Math.Pow(AgaConstants.K_Size[j],5));
                    }

                    if (AgaConstants.U_BinaryInteration[i][j] != 1)
                    {
                        U5PO = U5PO + XIJ * (Math.Pow(AgaConstants.U_BinaryInteration[i][j], 5) - 1) * Math.Sqrt((Math.Pow(AgaConstants.E_Energy[i], 5) * Math.Pow(AgaConstants.E_Energy[j], 5)));                            
                    }

                    if (AgaConstants.G_BinaryInteration[i][j] != 1)
                    {
                        WW = WW + XIJ * (AgaConstants.G_BinaryInteration[i][j] - 1) * ((AgaConstants.G_Orientation[i] + AgaConstants.G_Orientation[j]) / 2);
                    }

                    EIJ = AgaConstants.E_BinaryInteration[i][j] * Math.Sqrt(AgaConstants.E_Energy[i] * AgaConstants.E_Energy[j]);
                    WIJ = AgaConstants.G_BinaryInteration[i][j] * (AgaConstants.G_Orientation[i] + AgaConstants.G_Orientation[j]) / 2;
                    EOP5 = Math.Sqrt(EIJ);
                    E2PO = EIJ * EIJ;
                    E3PO = EIJ*E2PO;
                    E3P5 = E3PO*EOP5;
                    E4P5 = EIJ*E3P5;
                    E6PO = E3PO*E3PO;
                    E11PO = E4P5*E4P5*E2PO;
                    E7P5 = E4P5*EIJ*E2PO;
                    E9P5 = E7P5*E2PO;
                    E12PO = E11PO*EIJ;
                    E12P5 = E12PO*EOP5;
                    S3 = XIJ*Math.Sqrt(Math.Pow(AgaConstants.K_Size[i],3)*Math.Pow(AgaConstants.K_Size[j],3));
                    B[0] = B[0] + S3;
                    B[1] = B[1] + S3*EOP5;
                    B[2] = B[2] + S3*EIJ;
                    B[3] = B[3] + S3*E3P5;
                    B[4] = B[4] + S3*WIJ/EOP5;
                    B[5] = B[5] + S3*WIJ*E4P5;
                    B[6] = B[6] + S3*AgaConstants.Q_Quadrupole[i]*AgaConstants.Q_Quadrupole[j]*EOP5;
                    B[7] = B[7] + S3*AgaConstants.S_Dipole[i]*AgaConstants.S_Dipole[j]*E7P5;
                    B[8] = B[8] + S3*AgaConstants.S_Dipole[i]*AgaConstants.S_Dipole[j]*E9P5;
                    B[9] = B[9] + S3*AgaConstants.W_Association[i]*AgaConstants.W_Association[j]*E6PO;
                    B[10] = B[10] + S3*AgaConstants.W_Association[i]*AgaConstants.W_Association[j]*E12PO;
                    B[11] = B[11] + S3*AgaConstants.W_Association[i]*AgaConstants.W_Association[j]*E12P5;
                    B[12] = B[12] + S3*AgaConstants.F_HighTemp[i]*AgaConstants.F_HighTemp[j]/E6PO;
                    B[13] = B[13] + S3*E2PO;
                    B[14] = B[14] + S3*E3PO;
                    B[15] = B[15] + S3*AgaConstants.Q_Quadrupole[i]*AgaConstants.Q_Quadrupole[j]*E2PO;
                    B[16] = B[16] + S3*E2PO;
                    B[17] = B[17] + S3 * E11PO;
                }
            }

            for (int i = 0; i<B.Length; i++)
            {
                B[i] = B[i]*AgaConstants.a_EquationOfState[i];
            }

            RK3PO = Math.Pow((RK5PO + RK2P5*RK2P5),0.6);
            UU = Math.Pow((U5PO + U2P5*U2P5),0.2);
            Q2PO = Q1PO*Q1PO;


            Temp(temperatureInKelvin);

            double PF = pressurePSI*6894.757/1000000;
            double DF = DDetail(PF, temperatureInKelvin);
            double ZF = ZDetail(DF, temperatureInKelvin);

            Aga8Result aga8CalcResult = new Aga8Result();
            int decimalPlaces = 10;

#if DEMO
            decimalPlaces = 0;
#endif

            aga8CalcResult.FlowCompressiblity = Math.Round(ZF, decimalPlaces);
            aga8CalcResult.SpecificGravity = Math.Round(molarMass(gasComps) / 28.9625, decimalPlaces);

            return aga8CalcResult;
        }

        private double molarMass(double [] gasComps)
        {
            double mass = 0;

            for (int i = 0; i < gasComps.Length; i++)
            {
                mass = mass + (AgaConstants.MolarMass[i]*gasComps[i]);
            }
      
            return mass;
        }

        private double DDetail(double Pressure, double TempInKevin)
        {
            double EPSP, EPSR, EPSMIN;
            double DDETAIL, PDETAIL, X1, X2, X3, Y1, Y2, Y3;
            double DELX, DELPRV, DELMIN, DELBIS, XNUMBER, XDENOM, SGNDEL;
            double Y2MY3, Y3MY1, Y1MY2, BOUNDN;

            int IMAX = 150;

             RHO = 0;
             RHOL = 0;
             RHOH = 0;
             PRHOL = 0;
             PRHOH = 0;

            EPSP = 0.000001;
            EPSR = 0.000001;
            EPSMIN = 0.0000001;
            
            BRACKET(TempInKevin, Pressure);

            if (CODE == 1 || CODE == 3)
            {
               return RHO;
            }

            X1 = RHOL;
            X2 = RHOH;
            Y1 = PRHOL - Pressure;
            Y2 = PRHOH - Pressure;
            DELX = X1 - X2;
            DELPRV = DELX;

            X3 = X1;
            Y3 = Y1;

            for (int i = 0; i < IMAX; i++)
            {
                if ((Y2 * Y3) > 0)
                {
                    X3 = X1;
                    Y3 = Y1;
                    DELX = X1 - X2;
                    DELPRV = DELX;
                }
                if (Math.Abs(Y3) < Math.Abs(Y2))
                {
                    X1 = X2;
                    X2 = X3;
                    X3 = X1;
                    Y1 = Y2;
                    Y2 = Y3;
                    Y3 = Y1;
                }
                DELMIN = EPSMIN * Math.Abs(X2);

                DELBIS = 0.5 * (X3 - X2);

                if (Math.Abs(DELPRV) < DELMIN || Math.Abs(Y1) < Math.Abs(Y2))
                {
                    DELX = DELBIS;
                    DELPRV = DELBIS;
                }
                else
                {
                    if (X3 != X1)
                    {
                        Y2MY3 = Y2 - Y3;
                        Y3MY1 = Y3 - Y1;
                        Y1MY2 = Y1 - Y2;
                        XDENOM = -(Y1MY2) * (Y2MY3) * (Y3MY1);
                        XNUMBER = X1 * Y2 * Y3 * (Y2MY3) + X2 * Y3 * Y1 * (Y3MY1)
                            + X3 * Y1 * Y2 * (Y1MY2) - X2 * XDENOM;
                    }
                    else
                    {
                        XNUMBER = (X2 - X1) * Y2;
                        XDENOM = Y1 - Y2;
                    }

                    if (2 * Math.Abs(XNUMBER) < Math.Abs(DELPRV * XDENOM))
                    {
                        DELPRV = DELX;
                        DELX = XNUMBER / XDENOM;
                    }
                    else
                    {
                        DELX = DELBIS;
                        DELPRV = DELBIS;
                    }

                    if ((Math.Abs(Y2) < EPSP * Pressure) && Math.Abs(DELX) < EPSR * Math.Abs(X2))
                    {
                        return X2 + DELX;
                    }
                }

                if (Math.Abs(DELX) < DELMIN)
                {
                    SGNDEL = DELBIS / Math.Abs(DELBIS);
                    DELX = 1.0000009 * SGNDEL * DELMIN;
                    DELPRV = DELX;
                }

                BOUNDN = DELX * (X2 + DELX - X3);

                if (BOUNDN > 0)
                {
                    DELX = DELBIS;
                    DELPRV = DELBIS;
                }

                X1 = X2;
                Y1 = Y2;
                X2 = X2 + DELX;
                Y2 = PDetail(X2, TempInKevin) - Pressure;
            }

            //If we exceed max number, then return X2
            return X2;

        }

        private void BRACKET(double TemperatureInKelvin, double Pressure)
        {
            CODE = 0;
            int IMAX = 200;

             RHOL = 0;
             PRHOL = 0;
             RHOH = 0;
             PRHOH = 0;
             RHO = 0; 


            double RHO1 = 0;
            double RHO2 = 0;
            double P1 = 0;
            double P2 = 0;
            double RHOMAX = 1 / RK3PO;
            double VIDEAL = 0;
            
            double DEL = 0;

            if (TemperatureInKelvin == 330.15)
            {
                int fdslkj = 2;
            }

            if (TemperatureInKelvin > 1.2593 * UU)
            {
                RHOMAX = 20 * RHOMAX;     
            }

            VIDEAL = RGAS * TemperatureInKelvin / Pressure;

            if (Math.Abs(BMIX) < (0.167 * VIDEAL))
            {
                RHO2 = 0.95 / (VIDEAL + BMIX);
            }
            else
            {
                RHO2 = 1.15 / VIDEAL;
            }

            DEL = RHO2 / 20;
            int IT = 0;

            while (true)
            {
                IT++;
                if (IT > IMAX)
                {
                    CODE = 3;
                    RHO = RHO2;
                    return;
                }

                if (CODE != 2 && RHO2 > RHOMAX)
                {
                    CODE = 2;
                    DEL = 0.01 * (RHOMAX - RHO1) + Pressure / (RGAS * TemperatureInKelvin) / 20;
                    RHO2 = RHO1 + DEL;
                }
                else
                {
                    P2 = PDetail(RHO2, TemperatureInKelvin);

                    if (P2 > Pressure)
                    {
                        RHOL = RHO1;
                        PRHOL = P1;
                        RHOH = RHO2;
                        PRHOH = P2;
                        return;
                    }
                    else if (P2 > P1 && CODE == 2)
                    {
                        RHO1 = RHO2;
                        P1 = P2;
                        RHO2 = RHO1 + DEL;                     
                    }
                    else if (P2 > P1 && CODE == 0)
                    {
                        DEL = 2 * DEL;
                        RHO1 = RHO2;
                        P1 = P2;
                        RHO2 = RHO1 + DEL;
                    }
                    else
                    {
                        CODE = 1;
                        RHO = RHO1;
                        return;
                    }
                }
            }
        }

        private double PDetail(double Rho,double temp)
        {
            return ZDetail(Rho, temp) * Rho * RGAS * temp;
        }

        private double ZDetail(double density, double temperatureInKelvin)
        {
            double D1, D2, D3, D4, D5, D6, D7, D8, D9, EXP1, EXP2, EXP3, EXP4;

            if (temperatureInKelvin != TOLD)
            {
                Temp(temperatureInKelvin);
            }

            D1 = RK3PO*density;
            D2 = D1 * D1;
            D3 = D2 * D1;
            D4 = D3 * D1;
            D5 = D4 * D1;
            D6 = D5 * D1;
            D7 = D6 * D1;
            D8 = D7 * D1;
            D9 = D8 * D1;

            EXP1 = Math.Exp(-D1);
            EXP2 = Math.Exp(-D2);
            EXP3 = Math.Exp(-D3);
            EXP4 = Math.Exp(-D4);


            double result = 1 + BMIX * density +
                FN[12] * D1 * (EXP3 - 1 - 3 * D3 * EXP3) +
                (FN[13] + FN[14] + FN[15]) * D1 * (EXP2 - 1 - 2 * D2 * EXP2) +
                (FN[16] + FN[17]) * D1 * (EXP4 - 1 - 4 * D4 * EXP4) +
                (FN[18] + FN[19]) * D2 * 2 +
                (FN[20] + FN[21] + FN[22]) * D2 * (2 - 2 * D2) * EXP2 +
                (FN[23] + FN[24] + FN[25]) * D2 * (2 - 4 * D4) * EXP4 +
                FN[26] * D2 * (2 - 4 * D4) * EXP4 +
                FN[27] * D3 * 3 +
                (FN[28] + FN[29]) * D3 * (3 - D1) * EXP1 +
                (FN[30] + FN[31]) * D3 * (3 - 2 * D2) * EXP2 +
                (FN[32] + FN[33]) * D3 * (3 - 3 * D3) * EXP3 +
                (FN[34] + FN[35] + FN[36]) * D3 * (3 - 4 * D4) * EXP4 +
                (FN[37] + FN[38]) * D4 * 4 +
                (FN[39] + FN[40] + FN[41]) * D4 * (4 - 2 * D2) * EXP2 +
                (FN[42] + FN[43]) * D4 * (4 - 4 * D4) * EXP4 +
                FN[44] * D5 * 5 +
                (FN[45] + FN[46]) * D5 * (5 - 2 * D2) * EXP2 +
                (FN[47] + FN[48]) * D5 * (5 - 4 * D4) * EXP4 +
                FN[49] * D6 * 6 +
                FN[50] * D6 * (6 - 2 * D2) * EXP2 +
                FN[51] * D7 * 7 +
                FN[52] * D7 * (7 - 2 * D2) * EXP2 +
                FN[53] * D8 * (8 - D1) * EXP1 +
                (FN[54] + FN[55]) * D8 * (8 - 2 * D2) * EXP2 +
                (FN[56] + FN[57]) * D9 * (9 - 2 * D2) * EXP2;


                return result;

        }

        private double BMix(double tempInKelvin)
        {
            double TOP5, T2PO, T3PO, T3P5, T4P5, T6PO, T11PO, T, T7P5, T9P5, T12PO, P12P5, T12P5;

            TOP5 = Math.Sqrt(tempInKelvin);
            T2PO = tempInKelvin * tempInKelvin;
            T3PO = tempInKelvin * T2PO;
            T3P5 = T3PO * TOP5;
            T4P5 = tempInKelvin * T3P5;
            T6PO = T3PO * T3PO;
            T11PO = T4P5 * T4P5 * T2PO;
            T7P5 = T6PO * tempInKelvin * TOP5;
            T9P5 = T7P5 * T2PO;
            T12PO = T9P5 * TOP5 * T2PO;
            T12P5 = T12PO * TOP5;

            BMIX = B[0] +
                B[1] / TOP5 +
                B[2] / tempInKelvin +
                B[3] / T3P5 +
                B[4] * TOP5 +
                B[5] / T4P5 +
                B[6] / TOP5 +
                B[7] / T7P5 +
                B[8] / T9P5 +
                B[9] / T6PO +
                B[10] / T12PO +
                B[11] / T12P5 +
                B[12] * T6PO +
                B[13] / T2PO +
                B[14] / T3PO +
                B[15] / T2PO +
                B[16] / T2PO +
                B[17] / T11PO;
            return BMIX;
        }

        private double Temp(double temperatureInKelvin)
        {
            FN = new double[58];
            
            double TROP5;
            double TR1P5;
            double TR2PO;
            double TR3PO;
            double TR4PO;
            double TR5PO;
            double TR6PO;
            double TR7PO;
            double TR8PO;
            double TR9PO;
            double TR11PO;
            double TR13PO;
            double TR21PO;
            double TR22PO;
            double TR23PO;

            double TR;

            BMix(temperatureInKelvin);

            TR = temperatureInKelvin / UU;
            TROP5 = Math.Sqrt(TR);
            TR1P5 = TR * TROP5;
            TR2PO = TR * TR;
            TR3PO = TR * TR2PO;
            TR4PO = TR * TR3PO;
            TR5PO = TR * TR4PO;
            TR6PO = TR * TR5PO;
            TR7PO = TR * TR6PO;
            TR8PO = TR * TR7PO;
            TR9PO = TR * TR8PO;
            TR11PO = TR6PO * TR5PO;
            TR13PO = TR6PO * TR7PO;
            TR21PO = TR9PO * TR9PO * TR3PO;
            TR22PO = TR * TR21PO;
            TR23PO = TR * TR22PO;

            FN[12] = AgaConstants.a_EquationOfState[12] * HH * TR6PO;
            FN[13] = AgaConstants.a_EquationOfState[13] / TR2PO;
            FN[14] = AgaConstants.a_EquationOfState[14] / TR3PO;
            FN[15] = AgaConstants.a_EquationOfState[15] * Q2PO / TR2PO;
            FN[16] = AgaConstants.a_EquationOfState[16] / TR2PO;
            FN[17] = AgaConstants.a_EquationOfState[17] / TR11PO;
            FN[18] = AgaConstants.a_EquationOfState[18] * TROP5;
            FN[19] = AgaConstants.a_EquationOfState[19] / TROP5;
            FN[20] = AgaConstants.a_EquationOfState[20];
            FN[21] = AgaConstants.a_EquationOfState[21] / TR4PO;
            FN[22] = AgaConstants.a_EquationOfState[22] / TR6PO;
            FN[23] = AgaConstants.a_EquationOfState[23] / TR21PO;
            FN[24] = AgaConstants.a_EquationOfState[24] * WW / TR23PO;
            FN[25] = AgaConstants.a_EquationOfState[25] * Q2PO / TR22PO;
            FN[26] = AgaConstants.a_EquationOfState[26] * HH * TR;
            FN[27] = AgaConstants.a_EquationOfState[27] * Q2PO * TROP5;
            FN[28] = AgaConstants.a_EquationOfState[28] * WW / TR7PO;
            FN[29] = AgaConstants.a_EquationOfState[29] * HH * TR;
            FN[30] = AgaConstants.a_EquationOfState[30] / TR6PO;
            FN[31] = AgaConstants.a_EquationOfState[31] * WW / TR4PO;
            FN[32] = AgaConstants.a_EquationOfState[32] * WW / TR;
            FN[33] = AgaConstants.a_EquationOfState[33] * WW / TR9PO;
            FN[34] = AgaConstants.a_EquationOfState[34] * HH * TR13PO;
            FN[35] = AgaConstants.a_EquationOfState[35] / TR21PO;
            FN[36] = AgaConstants.a_EquationOfState[36] * Q2PO / TR21PO;
            FN[37] = AgaConstants.a_EquationOfState[37] * TROP5;
            FN[38] = AgaConstants.a_EquationOfState[38];
            FN[39] = AgaConstants.a_EquationOfState[39] / TR2PO;
            FN[40] = AgaConstants.a_EquationOfState[40] / TR7PO;
            FN[41] = AgaConstants.a_EquationOfState[41] * Q2PO / TR9PO;
            FN[42] = AgaConstants.a_EquationOfState[42] / TR22PO;
            FN[43] = AgaConstants.a_EquationOfState[43] / TR23PO;
            FN[44] = AgaConstants.a_EquationOfState[44] / TR;
            FN[45] = AgaConstants.a_EquationOfState[45] / TR9PO;
            FN[46] = AgaConstants.a_EquationOfState[46] * Q2PO / TR3PO;
            FN[47] = AgaConstants.a_EquationOfState[47] / TR8PO;
            FN[48] = AgaConstants.a_EquationOfState[48] * Q2PO / TR23PO;
            FN[49] = AgaConstants.a_EquationOfState[49] / TR1P5;
            FN[50] = AgaConstants.a_EquationOfState[50] * WW / TR5PO;
            FN[51] = AgaConstants.a_EquationOfState[51] * Q2PO * TROP5;
            FN[52] = AgaConstants.a_EquationOfState[52]/TR4PO;
            FN[53] = AgaConstants.a_EquationOfState[53] * WW / TR7PO;
            FN[54] = AgaConstants.a_EquationOfState[54] / TR3PO;
            FN[55] = AgaConstants.a_EquationOfState[55] * WW;
            FN[56] = AgaConstants.a_EquationOfState[56] / TR;
            FN[57] = AgaConstants.a_EquationOfState[57] * Q2PO;

            return 0;
        }
    }
}
