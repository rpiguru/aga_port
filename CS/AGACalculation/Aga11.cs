﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AGAFlowCalculation
{

    public struct AGA11Result
    {
        public double Rho;
        public double SpecificGravity;
        public double FlowingCompressibility_Zf;
        public double BaseCompressibility_Zb;
        public double Compressibility_FPV;
        public double VolumetricFlowRate_Qb;
        public double VolumetricFlowRate_Qf;
    }

    public class Aga11
    {
        internal const double A = 4.343524261523267;
        internal const double B = 3.764387693320165;


        internal const double A0 = 0.5961;
        internal const double A1 = 0.0291;
        internal const double A2 = -0.229;
        internal const double A3 = 0.003;
        internal const double A4 = 2.8;
        internal const double A5 = 0.000511;
        internal const double A6 = 0.021;

        internal const double S1 = 0.0049;
        internal const double S2 = 0.0433;
        internal const double S3 = 0.0712;
        internal const double S4 = -0.1145;
        internal const double S5 = -0.2300;
        internal const double S6 = -0.0116;
        internal const double S7 = -0.5200;
        internal const double S8 = -0.1400;

        internal const double k = 1.3;

        internal const double AirConstant = 28.9625;

        internal const double Rgas = 0.0831451;
        internal const double Nc = 0.036;
        internal const double Nic = 0.1;
        internal const double N3 = 1000.0;
        internal const double N4 = 25.4;
        internal const double N5 = 273.15;

        internal const double Xc = 1.142129337256165;

        internal const double meu = 0.010268;

/// <summary>
/// Calculates AGA11 Flow based using gas compositions and AGA8 calcuations
/// Pressure units are in KPA
/// Temperature units are in Celcius
/// Length units are in mm
/// </summary>
/// <param name="flowTemperatureInCelcius">Flowing Temperature (C)</param>
/// <param name="pipeReferenceTemperatureInCelcius">Pipe Reference Temperature (C)</param>
/// <param name="orificeReferenceTemperatureInCelcius">Orifice Reference Temperature (C)</param>
/// <param name="baseTemperatureInCelcius">Base Temperature (C)</param>
/// <param name="staticPressureKPA">Static Pressure (KPA)</param>
/// <param name="baseStaticPressureKPA">Base Static Pressure (KPA)</param>
/// <param name="differentialPressureKPA">Differential Pressure (KPA)</param>
/// <param name="orificeSizeMM">Orifice Size (mm)</param>
/// <param name="pipeSizeMM">Pipe Size (mm)</param>
/// <param name="orificeMaterial">Orfice Material (Carbon, Monel, Stainless Steel)</param>
/// <param name="pipeMaterial">Pipe Material (Carbon, Monel, Stainless Steel)</param>
/// <param name="tapIsUpstream">upstream = true, downstream=false</param>
/// <param name="gasCompositions">Gas Composition Collection</param>
/// <returns></returns>
        public AGA11Result CalculateFlow(double flowTemperature,
            double baseTemperature, double staticPressure,
            double baseStaticPressure, double moleWeight, GasComps gasCompositions)
        {

            double staticPressBar = UnitConverter.KPAToBar(staticPressure);


            Aga8 ZCalc = new Aga8();
            Aga8Result calcZResult = ZCalc.CalculateZ(gasCompositions, UnitConverter.CELCIUStoKELVIN(flowTemperature), UnitConverter.KPAtoPSI(UnitConverter.BarToKPA(staticPressBar)));

            double FlowZCalc = calcZResult.FlowCompressiblity;
            double BaseZCalc = ZCalc.CalculateZ(gasCompositions, UnitConverter.CELCIUStoKELVIN(baseTemperature), UnitConverter.KPAtoPSI(baseStaticPressure)).FlowCompressiblity;
            double SpecificGravity = Math.Round(calcZResult.SpecificGravity, 4);

            AGA11Result aga11 =  CalculateFlow(UnitConverter.CELCIUStoKELVIN(flowTemperature), staticPressure,
                 moleWeight, FlowZCalc, BaseZCalc, SpecificGravity);

            return aga11;
        }


  /// <summary>
/// Calculates AGA3 Flow based pre-calculated compressibility and specific gravity
/// Pressure units are in KPA
/// Temperature units are in Celcius
/// Length units are in mm
/// </summary>
/// <param name="flowTemperatureInCelcius">Flowing Temperature (C)</param>
/// <param name="pipeReferenceTemperatureInCelcius">Pipe Reference Temperature (C)</param>
/// <param name="orificeReferenceTemperatureInCelcius">Orifice Reference Temperature (C)</param>
/// <param name="baseTemperatureInCelcius">Base Temperature (C)</param>
/// <param name="staticPressureKPA">Static Pressure (KPA)</param>
/// <param name="baseStaticPressureKPA">Base Static Pressure (KPA)</param>
/// <param name="differentialPressureKPA">Differential Pressure (KPA)</param>
/// <param name="orificeSizeMM">Orifice Size (mm)</param>
/// <param name="pipeSizeMM">Pipe Size (mm)</param>
/// <param name="orificeMaterial">Orfice Material (Carbon, Monel, Stainless Steel)</param>
/// <param name="pipeMaterial">Pipe Material (Carbon, Monel, Stainless Steel)</param>
/// <param name="tapIsUpstream">upstream = true, downstream=false</param>
/// <param name="flowCompressibility">Compressibility factor at flowing conditions</param>
/// <param name="baseCompressibility">Compressibility factor at base conditions</param>
/// <param name="specificGravity">Specific Gravity (Ideal gas relative density)</param>
  /// <returns></returns>
        public AGA11Result CalculateFlow(double flowTemperature, double baseTemperature, double staticPressure, 
            double baseStaticPressure, double moleWeight, double flowCompressibility, double baseCompressibility, double specificGravity)
        {
        
            return CalculateFlow(UnitConverter.CELCIUStoKELVIN(flowTemperature), staticPressure+baseStaticPressure,
                moleWeight, flowCompressibility, baseCompressibility, specificGravity);
        }

        /// <summary>
        /// Pressure units are in KPA
        /// Temperature units are in Kelvin
        /// Length units are in mm
        /// </summary>
        /// <param name="gasComps"></param>
        /// <param name="flowTemperatureInKelvin"></param>
        /// <param name="pipeReferenceTemperatureInKelvin"></param>
        /// <param name="orificeReferenceTemperatureInKelvin"></param>
        /// <param name="baseTemperatureInKelvin"></param>
        /// <param name="staticPressureKPA"></param>
        /// <param name="baseStaticPressureKPA"></param>
        /// <param name="differentialPressureKPA"></param>
        /// <param name="orificeSizeMM"></param>
        /// <param name="pipeSizeMM"></param>
        /// <param name="orificeExpansionCoefficient"></param>
        /// <param name="pipeExpansionCoefficient"></param>
        /// <param name="tapIsUpstream"></param>
        /// <returns></returns>
        private AGA11Result CalculateFlow(double flowTemperatureInKelvin,
         double staticAbsolutePressureKPA, double moleWeight, double FlowZCalc, double BaseZCalc, double SpecificGravity)
        {
            AGA11Result results = new AGA11Result();

            double P = staticAbsolutePressureKPA;
            double Tk = flowTemperatureInKelvin;
            double mw = moleWeight;
            double Z = BaseZCalc;
            double R = 8.31432;

            double Rho = P * mw / (Tk * Z * R);


            results.Rho = Rho;
            results.Compressibility_FPV = Math.Sqrt(BaseZCalc / FlowZCalc);// Math.Round(Math.Sqrt(BaseZCalc / FlowZCalc), 0);
            results.SpecificGravity = SpecificGravity;
            results.BaseCompressibility_Zb = BaseZCalc;
            results.FlowingCompressibility_Zf = FlowZCalc;

            return results;
        }
    }
}
