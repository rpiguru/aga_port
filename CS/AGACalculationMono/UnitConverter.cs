﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AGAFlowCalculation
{
    public static class UnitConverter
    {
        public static double BarToMillibar(double bar)
        {
            return bar * 1000;
        }

        public static double MillibarToBar(double millibar)
        {
            if (millibar == 0)
            {
                return 0;
            }

            return millibar / 1000;
        }

        public static double BarToKPA(double bar)
        {
            return bar * 100;
        }

        public static double KPAToBar(double kpa)
        {
            if (kpa == 0)
            {
                return 0;
            }
            return kpa / 100;
        }

        public static double ft3Tom3(double ft3)
        {

            return ft3 / 35.315;
        }

        public static double MillibarToKPA(double millibar)
        {
            if (millibar == 0)
            {
                return 0;
            }
            return millibar / 10;
        }

        public static double KPAToMillibar(double kpa)
        {
            return kpa * 10;
        }

        public static double MCFtoE3M3(double mcf)
        {
            return mcf * 0.028174;
        }

        public static double E3M3toMFC(double e3m3)
        {
            if (e3m3 == 0)
            {
                return 0;
            }
            return e3m3 / 0.028174;
        }



        public static double CELCIUStoFARHENHEIT(double celcius)
        {
            return (celcius * 9 / 5) + 32;
        }

        public static double FARHENHEITtoCELCIUS(double farhenheit)
        {
            return (farhenheit - 32) * 0.55556;
        }
        
        public static double CELCIUStoKELVIN(double celcius)
        {
            return (celcius + 273.15);
        }

        public static double InH2OtoKPA(double inH2O)
        {
            if (inH2O == 0)
            {
                return 0;
            }
            return (inH2O * 0.249088908);
        }

        public static double KPAtoINH2O(double kpa)
        {
            if (kpa == 0)
            {
                return 0;
            }
            return (kpa * 4.014630786);
        }
  

        public static double PSItoKPA(double psi)
        {
            return (psi * 6.8948);
        }

        public static double KPAtoPSI(double kpa)
        {
            if (kpa == 0)
            {
                return 0;
            }

            return (kpa / 6.8948);
        }


        public static double INCHtoMM(double inch)
        {
            return (inch * 25.4);
           
        }

        public static double MMtoINCH(double mm)
        {
            return (mm * 0.0393700787402);
        }

    }
}
