﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AGAFlowCalculation
{
    public class GasComps
    {
        private double[] gasComps = new double[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

        public double[] GasCompositionArray
        {
            get { return this.gasComps; }
        }

        public GasComps(double methane, double nitrogen, double carbonDioxide,
           double ethane, double propane, double water, double hydrogenSulfide,
            double hydrogen, double carbonMonoxide, double oxygen, double iButane,
            double nButane, double iPentane, double nPentane, double nHexane,
            double nHeptane, double nOctane, double nNonane, double nDecane,
            double helium, double argon)
        {
            gasComps[0] = methane;
            gasComps[1] = nitrogen;
            gasComps[2] = carbonDioxide;
            gasComps[3] = ethane;
            gasComps[4] = propane;
            gasComps[5] = water;
            gasComps[6] = hydrogenSulfide;
            gasComps[7] = hydrogen;
            gasComps[8] = carbonMonoxide;
            gasComps[9] = oxygen;
            gasComps[10] = iButane;
            gasComps[11] = nButane;
            gasComps[12] = iPentane;
            gasComps[13] = nPentane;
            gasComps[14] = nHexane;
            gasComps[15] = nHeptane;
            gasComps[16] = nOctane;
            gasComps[17] = nNonane;
            gasComps[18] = nDecane;
            gasComps[19] = helium;
            gasComps[20] = argon;
        }

        public GasComps()
        {
            Array.Clear(gasComps, 0, gasComps.Length);
        }

        public void Add(double gasValue, AgaConstants.GasCompositions gasComp)
        {
            this.gasComps[(int)gasComp] = gasValue;
        }

        public double TotalComposition()
        {
            double totalGasComp= 0 ;
            foreach (double comp in this.gasComps)
            {
                totalGasComp += comp;
            }
            return totalGasComp; 
        }

        public bool IsGasCompositionValid()
        {
            double total = TotalComposition();
            if (total > .99 && total < 1.01)
                return true;
            else
                return false;
        }
    }
}
