﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AGAFlowCalculation
{

    public struct AGA3Result
    {
        public double Flow;
        public double SpecificGravity;
        public double FlowingCompressibility_Zf;
        public double BaseCompressibility_Zb;
        public double Compressibility_FPV;
    }

    public class Aga3
    {
        internal const double A = 4.343524261523267;
        internal const double B = 3.764387693320165;


        internal const double A0 = 0.5961;
        internal const double A1 = 0.0291;
        internal const double A2 = -0.229;
        internal const double A3 = 0.003;
        internal const double A4 = 2.8;
        internal const double A5 = 0.000511;
        internal const double A6 = 0.021;

        internal const double S1 = 0.0049;
        internal const double S2 = 0.0433;
        internal const double S3 = 0.0712;
        internal const double S4 = -0.1145;
        internal const double S5 = -0.2300;
        internal const double S6 = -0.0116;
        internal const double S7 = -0.5200;
        internal const double S8 = -0.1400;

        internal const double k = 1.3;

        internal const double AirConstant = 28.9625;

        internal const double Rgas = 0.0831451;
        internal const double Nc = 0.036;
        internal const double Nic = 0.1;
        internal const double N3 = 1000.0;
        internal const double N4 = 25.4;
        internal const double N5 = 273.15;

        internal const double Xc = 1.142129337256165;

        internal const double meu = 0.010268;

/// <summary>
/// Calculates AGA3 Flow based using gas compositions and AGA8 calcuations
/// Pressure units are in KPA
/// Temperature units are in Celcius
/// Length units are in mm
/// </summary>
/// <param name="flowTemperatureInCelcius">Flowing Temperature (C)</param>
/// <param name="pipeReferenceTemperatureInCelcius">Pipe Reference Temperature (C)</param>
/// <param name="orificeReferenceTemperatureInCelcius">Orifice Reference Temperature (C)</param>
/// <param name="baseTemperatureInCelcius">Base Temperature (C)</param>
/// <param name="staticPressureKPA">Static Pressure (KPA)</param>
/// <param name="baseStaticPressureKPA">Base Static Pressure (KPA)</param>
/// <param name="differentialPressureKPA">Differential Pressure (KPA)</param>
/// <param name="orificeSizeMM">Orifice Size (mm)</param>
/// <param name="pipeSizeMM">Pipe Size (mm)</param>
/// <param name="orificeMaterial">Orfice Material (Carbon, Monel, Stainless Steel)</param>
/// <param name="pipeMaterial">Pipe Material (Carbon, Monel, Stainless Steel)</param>
/// <param name="tapIsUpstream">upstream = true, downstream=false</param>
/// <param name="gasCompositions">Gas Composition Collection</param>
/// <returns></returns>
        public AGA3Result CalculateFlow(double flowTemperature, double pipeReferenceTemperature,
            double orificeReferenceTemperature, double baseTemperature, double staticPressure,
            double baseStaticPressure, double differentialPressure, double orificeSize, double pipeSize,
            AgaConstants.MaterialType orificeMaterial, AgaConstants.MaterialType pipeMaterial, bool tapIsUpstream,
            GasComps gasCompositions)
        {
            double orificeExpansionCoefficient = AgaConstants.thermalExpansionValuesLookup[(int)orificeMaterial];
            double pipeExpansionCoefficient = AgaConstants.thermalExpansionValuesLookup[(int)pipeMaterial];
            double diffPressMillibar = UnitConverter.KPAToMillibar(differentialPressure);
            double staticPressBar = UnitConverter.KPAToBar(staticPressure);
            if (tapIsUpstream == false)
            {
                staticPressBar = staticPressBar + UnitConverter.MillibarToBar(diffPressMillibar);
            }

            Aga8 ZCalc = new Aga8();
            Aga8Result calcZResult = ZCalc.CalculateZ(gasCompositions, UnitConverter.CELCIUStoKELVIN(flowTemperature), UnitConverter.KPAtoPSI(UnitConverter.BarToKPA(staticPressBar)));

            double FlowZCalc = calcZResult.FlowCompressiblity;
            double BaseZCalc = ZCalc.CalculateZ(gasCompositions, UnitConverter.CELCIUStoKELVIN(baseTemperature), UnitConverter.KPAtoPSI(baseStaticPressure)).FlowCompressiblity;
            double SpecificGravity = Math.Round(calcZResult.SpecificGravity, 4);

            AGA3Result aga3 =  CalculateFlow(UnitConverter.CELCIUStoKELVIN(flowTemperature),
                UnitConverter.CELCIUStoKELVIN(pipeReferenceTemperature), UnitConverter.CELCIUStoKELVIN(orificeReferenceTemperature),
                UnitConverter.CELCIUStoKELVIN(baseTemperature), staticPressure, baseStaticPressure, differentialPressure, orificeSize,
                pipeSize, orificeExpansionCoefficient, pipeExpansionCoefficient, tapIsUpstream, FlowZCalc, BaseZCalc, SpecificGravity);

            return aga3;
        }


  /// <summary>
/// Calculates AGA3 Flow based pre-calculated compressibility and specific gravity
/// Pressure units are in KPA
/// Temperature units are in Celcius
/// Length units are in mm
/// </summary>
/// <param name="flowTemperatureInCelcius">Flowing Temperature (C)</param>
/// <param name="pipeReferenceTemperatureInCelcius">Pipe Reference Temperature (C)</param>
/// <param name="orificeReferenceTemperatureInCelcius">Orifice Reference Temperature (C)</param>
/// <param name="baseTemperatureInCelcius">Base Temperature (C)</param>
/// <param name="staticPressureKPA">Static Pressure (KPA)</param>
/// <param name="baseStaticPressureKPA">Base Static Pressure (KPA)</param>
/// <param name="differentialPressureKPA">Differential Pressure (KPA)</param>
/// <param name="orificeSizeMM">Orifice Size (mm)</param>
/// <param name="pipeSizeMM">Pipe Size (mm)</param>
/// <param name="orificeMaterial">Orfice Material (Carbon, Monel, Stainless Steel)</param>
/// <param name="pipeMaterial">Pipe Material (Carbon, Monel, Stainless Steel)</param>
/// <param name="tapIsUpstream">upstream = true, downstream=false</param>
/// <param name="flowCompressibility">Compressibility factor at flowing conditions</param>
/// <param name="baseCompressibility">Compressibility factor at base conditions</param>
/// <param name="specificGravity">Specific Gravity (Ideal gas relative density)</param>
  /// <returns></returns>
        public AGA3Result CalculateFlow(double flowTemperature, double pipeReferenceTemperature, 
            double orificeReferenceTemperature, double baseTemperature, double staticPressure, 
            double baseStaticPressure, double differentialPressure, double orificeSize, double pipeSize,
            AgaConstants.MaterialType orificeMaterial, AgaConstants.MaterialType pipeMaterial, bool tapIsUpstream,
            double flowCompressibility, double baseCompressibility, double specificGravity)
        {
            double orificeExpansionCoefficient = AgaConstants.thermalExpansionValuesLookup[(int)orificeMaterial];
            double pipeExpansionCoefficient = AgaConstants.thermalExpansionValuesLookup[(int)pipeMaterial];

            return CalculateFlow(UnitConverter.CELCIUStoKELVIN(flowTemperature),
             UnitConverter.CELCIUStoKELVIN(pipeReferenceTemperature), UnitConverter.CELCIUStoKELVIN(orificeReferenceTemperature),
             UnitConverter.CELCIUStoKELVIN(baseTemperature), staticPressure, baseStaticPressure, differentialPressure, orificeSize,
             pipeSize, orificeExpansionCoefficient, pipeExpansionCoefficient, tapIsUpstream,
             flowCompressibility, baseCompressibility, specificGravity);
        }

        /// <summary>
        /// Pressure units are in KPA
        /// Temperature units are in Kelvin
        /// Length units are in mm
        /// </summary>
        /// <param name="gasComps"></param>
        /// <param name="flowTemperatureInKelvin"></param>
        /// <param name="pipeReferenceTemperatureInKelvin"></param>
        /// <param name="orificeReferenceTemperatureInKelvin"></param>
        /// <param name="baseTemperatureInKelvin"></param>
        /// <param name="staticPressureKPA"></param>
        /// <param name="baseStaticPressureKPA"></param>
        /// <param name="differentialPressureKPA"></param>
        /// <param name="orificeSizeMM"></param>
        /// <param name="pipeSizeMM"></param>
        /// <param name="orificeExpansionCoefficient"></param>
        /// <param name="pipeExpansionCoefficient"></param>
        /// <param name="tapIsUpstream"></param>
        /// <returns></returns>
        private AGA3Result CalculateFlow(double flowTemperatureInKelvin,
            double pipeReferenceTemperatureInKelvin, double orificeReferenceTemperatureInKelvin, 
            double baseTemperatureInKelvin, double staticPressureKPA, double baseStaticPressureKPA, 
            double differentialPressureKPA, double orificeSizeMM, double pipeSizeMM, double orificeExpansionCoefficient,
            double pipeExpansionCoefficient, bool tapIsUpstream, double FlowZCalc, double BaseZCalc, double SpecificGravity)
        {
            double d = orificeSizeMM * (1 + orificeExpansionCoefficient * (flowTemperatureInKelvin - orificeReferenceTemperatureInKelvin));
            double D = pipeSizeMM * (1 + pipeExpansionCoefficient * (flowTemperatureInKelvin - pipeReferenceTemperatureInKelvin));
            double Beta = d / D;
            double Ev = 1 / Math.Sqrt(1 - Math.Pow(Beta, 4));
            double L1 = N4 / D;
            double L2 = N4 / D;
            double M2 = (2 * L2) / (1 - Beta);

            double Tu = (S2 + S3 * Math.Exp(-8.5 * L1) + S4 * Math.Exp(-6.0 * L1)) * (Math.Pow(Beta, 4) / (1 - Math.Pow(Beta, 4)));
            double Td = S6 * (M2 + S7 * Math.Pow(M2, 1.3)) * Math.Pow(Beta, 1.1);
            double Ts = 0;

             
            double diffPressMillibar = UnitConverter.KPAToMillibar(differentialPressureKPA);
            double  staticPressBar = UnitConverter.KPAToBar(staticPressureKPA);
            
            if (tapIsUpstream == false)
            {
                staticPressBar = staticPressBar + UnitConverter.MillibarToBar(diffPressMillibar);
            }
            
            if (D > (A4 * N4))
            {
                Ts = 0.0;                
            }
            else
            {
                Ts = A3 * (1 - Beta) * (A4 - (D / N4));
            }

            //XQ9211.pdf Page: 34
            //Procedure 4.3.2.5 Calculation of Flange-Tapped Orifice Plate
            //Coefficient of Discharge Constants

            double Cd0 = A0 + A1 * Math.Pow(Beta, 2) + A2 * Math.Pow(Beta, 8) + Tu + Td + Ts;
            double Cd1 = A5 * Math.Pow(Beta, 0.7) * Math.Pow(250, 0.7);
            double Cd2 = A6 * Math.Pow(Beta, 4) * Math.Pow(250, 0.35);
            double Cd3 = S1 * Math.Pow(Beta, 4) * Math.Pow(Beta, 0.8) * Math.Pow(4.75, 0.8) * Math.Pow(250, 0.35);
            double Cd4 = (S5 * Tu + S8 * Td) * Math.Pow(Beta, 0.8) * Math.Pow(4.75, 0.8);

            double X1 = diffPressMillibar / (N3 * staticPressBar);

            double Yp = (0.41 + 0.35 * Math.Pow(Beta, 4))/k;
            double Y = 1 - (Yp * X1);

            double Ftmp = Ev * Y * d * d;



            double density = ((SpecificGravity * AirConstant / Rgas) * staticPressBar) / (FlowZCalc * flowTemperatureInKelvin);

            //XQ9211.pdf Page: 37
            //Procedure 4.3.2.8 Calculation of Iteration Flow Factor
            
                double FIc = (4000 * Nic * D * meu) / Ftmp;
                double Fip = Math.Sqrt(2 * density*diffPressMillibar);

                double Fi = 0;

                if (FIc < 1000 * Fip && Fip > 0)
                {
                    Fi = FIc / Fip;
                }
                else
                {
                    Fi = 1000;
                }

                double Cdft = Cd0;
            
            double deltaCd = 2;

            for (int i = 0; i < 50 && Math.Abs(deltaCd) > 0.0000005; i++)
            {
                double X2 = Fi / Cdft;

                double Fc = 0;
                double Dc = 0;

                if (X2 < Xc)
                {
                    Fc = Cd0 + (Cd1 * Math.Pow(X2, 0.35) + Cd2 + Cd3 * Math.Pow(X2, 0.8)) * Math.Pow(X2, 0.35) + Cd4 * Math.Pow(X2, 0.8);
                    Dc = (0.7 * Math.Pow(Cd1, 0.35) + 0.35 * Cd2 + 1.15 * Math.Pow(X2, 0.8)) * Math.Pow(X2, 0.35) + 0.8 * Cd4 * Math.Pow(X1, 0.8);
                }
                else
                {
                    Fc = Cd0 + Cd1 * Math.Pow(X2, 0.7) + (Cd2 + Cd3 * Math.Pow(X2, 0.8)) * (A - (B / X2)) + Cd4 * Math.Pow(X1, 0.98);
                    Dc = 0.7 * Cd1 * Math.Pow(X2, 0.7) + (Cd2 + Cd3 * Math.Pow(X1, 0.8)) * B / X2 + 0.8 * Cd3 * (A - B / X2) * Math.Pow(X2, 0.8) + 0.8 * Cd4 * Math.Pow(X2, 0.8);
                }

                deltaCd = (Cdft - Fc) / (1 + (Dc / Cdft));

                Cdft = Cdft - deltaCd;
            }

            double densityBase = ((SpecificGravity * AirConstant / Rgas) * UnitConverter.KPAToBar(baseStaticPressureKPA)) / (BaseZCalc * baseTemperatureInKelvin);

            double mFLow = 24 * 3.14159 / 4 * Nc * (Ftmp / Y) * Cdft * Y * Math.Sqrt(2 * density * diffPressMillibar);

            double formattedFlow = (mFLow / densityBase)/1000;

            AGA3Result results = new AGA3Result();

            results.Flow = formattedFlow;
            results.Compressibility_FPV = Math.Sqrt(BaseZCalc / FlowZCalc);// Math.Round(Math.Sqrt(BaseZCalc / FlowZCalc), 0);
            results.SpecificGravity = SpecificGravity;
            results.BaseCompressibility_Zb = BaseZCalc;
            results.FlowingCompressibility_Zf = FlowZCalc;

            return results;
        }
    }
}
