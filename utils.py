"""
Utility script contains some functions.

"""
import logging


# create console handler and set level to debug
console_handler = logging.StreamHandler()
console_handler.setLevel(logging.DEBUG)

# create formatter
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
# add formatter to ch
console_handler.setFormatter(formatter)


def validate_days_value(val):
    """
    Check given value is validate or not.
     Value must be separated with '/', and also be the name of days.
     Correct value: Mon/Thu/Sun
    :param val:
    :return:
    """
    days_list = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']

    val_list = val.split('/')
    for v in val_list:
        if v not in days_list:
            return False

    return True

